import 'package:flutter/src/widgets/framework.dart';
import 'package:html_unescape/html_unescape.dart';

import '../../common/constants.dart';

class BannerModel {
  String id;
  String sku;
  String name;
  String image;
  int catalog_category_id;

  BannerModel.fromMagentoJson(Map<String, dynamic> parsedJson) {
    try {
      id        = "${parsedJson["id"]}";
      name      = parsedJson["name"];
      image     = parsedJson["image_url"] != null ? parsedJson["image_url"] : kDefaultImage;
      catalog_category_id = parsedJson["catalog_category_id"];
    } catch (e, trace) {
      print(e.toString());
      print(trace.toString());
    }
  }

  @override
  String toString() => 'BannerModel { id: $id  name: $name}';
}
