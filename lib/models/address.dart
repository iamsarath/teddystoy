import 'package:localstorage/localstorage.dart';

class Address {
  String firstName;
  String lastName;
  String email;
  String street;
  String city;
  String state;
  int state_id;
  String country;
  String phoneNumber;
  String zipCode;
  String mapUrl;
  String house_no;
  String block_no;
  String additionalinfo;
  String avenue;
  String paci;

  Address(
      {this.firstName,
      this.lastName,
      this.email,
      this.street,
      this.city,
      this.state,
      this.state_id,
      this.country,
      this.phoneNumber,
      this.zipCode,
      this.house_no,
      this.additionalinfo,
      this.avenue,
      this.paci,
      this.mapUrl});

  Address.fromJson(Map<String, dynamic> parsedJson) {
    firstName = parsedJson["first_name"];
    lastName = parsedJson["last_name"];
    street = parsedJson["address_1"];
    city = parsedJson["city"];
    state = parsedJson["state"];
    state_id = parsedJson["state_id"];
    country = parsedJson["country"];
    email = parsedJson["email"];
    final alphanumeric = RegExp(r'^[a-zA-Z0-9]+$');
    if (alphanumeric.hasMatch(firstName)) {
      phoneNumber = firstName;
    }
    //phoneNumber = parsedJson["phone"];
    zipCode = parsedJson["postcode"];
  }

  Address.fromMagentoJson(Map<String, dynamic> parsedJson) {
    firstName = parsedJson["firstname"];
    lastName = parsedJson["lastname"];
    street = parsedJson["street"][0];
    city = parsedJson["city"];
    state = parsedJson["region"];
    country = parsedJson["country_id"];
    email = parsedJson["email"];
    phoneNumber = parsedJson["telephone"];
    zipCode = parsedJson["postcode"];
  }

  Map<String, dynamic> toJson() {
    return {
      "first_name": firstName,
      "last_name": lastName,
      "address_1": street,
      "address_2": '',
      "city": city,
      "state": state,
      "state_id": state_id,
      "country": country,
      "email": email,
      "phone": phoneNumber,
      "postcode": zipCode,
      "mapUrl": mapUrl,
      "house_no": house_no,
      "block_no": block_no,
      "additionalinfo": additionalinfo,
      "avenue": avenue,
      "paci": paci,
    };
  }

  Address.fromLocalJson(Map<String, dynamic> json) {
    try {
      firstName = json['first_name'];
      lastName = json['last_name'];
      street = json['address_1'];
      city = json['city'];
      state = json['state'];
      state_id = json['state_id'];
      country = json['country'];
      email = json['email'];
      phoneNumber = json['phone'];
      zipCode = json['postcode'];
      mapUrl = json['mapUrl'];
      house_no = json['house_no'];
      block_no = json['block_no'];
      additionalinfo = json['additionalinfo'];
      avenue = json['avenue'];
      paci = json['paci'];
    } catch (e) {
      print(e.toString());
    }
  }

  Map<String, dynamic> toMagentodataJson() {
//    state_id = 656;
    return {
      "address": {
        "region": state,
        "country_id": country,
        "region_id": state_id,
        "region_code": 'A3',
        "street": [street],
        "firstname": firstName,
        "lastname": lastName,
        "email": email,
        "telephone": phoneNumber,
        "same_as_billing": 1,
        "custom_attributes": [
          {"attribute_code": "house_number", "value": house_no},
          {"attribute_code": "block_number", "value": block_no},
          {"attribute_code": "additionalinfo", "value": additionalinfo},
          {"attribute_code": "avenue", "value": avenue},
          {"attribute_code": "paci", "value": paci}
        ]
      }
    };
  }

  Map<String, dynamic> toMagentoJson() {
    return {
      "address": {
        "region": state,
        "country_id": country,
        "region_id": state_id,
        "street": [street],
        "postcode": zipCode,
        "city": city,
        "firstname": firstName,
        "lastname": lastName,
        "email": email,
        "telephone": phoneNumber,
        "same_as_billing": 1
      }
    };
  }

  Map<String, dynamic> toOpencartJson() {
    return {
      "zone_id": "1234",
      "country_id": country,
      "address_1": street,
      "address_2": "",
      "postcode": zipCode,
      "city": city,
      "firstname": firstName,
      "lastname": lastName,
      "email": email,
      "telephone": phoneNumber
    };
  }

  bool isValid() {
    return firstName.isNotEmpty &&
        lastName.isNotEmpty &&
        email.isNotEmpty &&
        street.isNotEmpty &&
        city.isNotEmpty &&
        state.isNotEmpty &&
        country.isNotEmpty &&
        phoneNumber.isNotEmpty;
  }

  toJsonEncodable() {
    return {
      "first_name": firstName,
      "last_name": lastName,
      "address_1": street,
      "address_2": '',
      "city": city,
      "state": state,
      "country": country,
      "email": email,
      "phone": phoneNumber,
      "postcode": zipCode,
      "house_no": house_no
    };
  }

  Future<void> saveToLocal() async {
    final LocalStorage storage = LocalStorage("address");
    try {
      final ready = await storage.ready;
      if (ready) {
        await storage.setItem('', toJson());
      }
    } catch (err) {
      print(err);
    }
  }

  Address.fromShopifyJson(Map<String, dynamic> json) {
    try {
      firstName = json['firstName'];
      lastName = json['lastName'];
      street = json['address1'];
      city = json['city'];
      state = json['pronvice'];
      country = json['country'];
      email = json['email'];
      phoneNumber = json['phone'];
      zipCode = json['zip'];

      mapUrl = json['mapUrl'];
    } catch (e) {
      print(e.toString());
    }
  }

  Map<String, dynamic> toShopifyJson() {
    return {
      "address": {
        "province": state,
        "country": country,
        "address1": street,
        "zip": zipCode,
        "city": city,
        "firstName": firstName,
        "lastName": lastName,
        "phone": phoneNumber,
      }
    };
  }

  @override
  String toString() {
    return country;
  }
}

class ListAddress {
  List<Address> list = [];

  toJsonEncodable() {
    return list.map((item) {
      return item.toJsonEncodable();
    }).toList();
  }
}
