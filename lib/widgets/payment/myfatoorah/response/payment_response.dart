import '../../myfatoorah/enums/other.dart';

class PaymentResponse {
  final PaymentStatus status;
  final String paymentId;

  PaymentResponse(this.status, [this.paymentId]);
  @override
  String toString() {
    return "Status: $status     PaymentId: $paymentId";
  }
}
