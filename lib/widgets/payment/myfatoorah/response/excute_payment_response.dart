import '../../myfatoorah/response/base_response.dart';

class ExcutePaymentResponse
    extends MyFatoorahResponse<ExcutePaymentResponseData> {
  ExcutePaymentResponse.fromJson(Map<String, dynamic> json)
      : super.fromJson(json);
  @override
  ExcutePaymentResponseData mapData(Map<String, dynamic> json) {
    return ExcutePaymentResponseData.fromJson(json);
  }
}

class ExcutePaymentResponseData {
  int invoiceId;
  bool isDirectPayment;
  String paymentURL;
  String customerReference;
  String userDefinedField;

  ExcutePaymentResponseData.fromJson(Map<String, dynamic> json) {
    invoiceId = int.tryParse(
        (json['InvoiceId'] ?? json['invoiceId'])?.toString() ?? "");
    isDirectPayment =
        json['IsDirectPayment'] == true || json['isDirectPayment'] == true;
    paymentURL = json['PaymentURL'] ?? json['paymentURL'] ?? "";
    customerReference =
        json['CustomerReference'] ?? json['customerReference'] ?? "";
    userDefinedField =
        json['UserDefinedField'] ?? json['userDefinedField'] ?? "";
  }
}
