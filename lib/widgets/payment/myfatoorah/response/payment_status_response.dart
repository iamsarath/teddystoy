import 'package:teddystoy/widgets/payment/myfatoorah/response/payment_status_invoice_attributes.dart';

class MFPaymentStatus {
  final bool IsSuccess;
  final String InvoiceId;
  final String InvoiceStatus;
  final String InvoiceReference;
  final List<PaymentStatusInvoiceAttributes> InvoiceTransactions;

//  final String TransactionDate;
//  final String TransactionId;
//  final String PaymentGateway;
//  final String TrackId;
//  final String PaymentId;
//  final String PaidCurrency;
//  final String PaidCurrencyValue;
//  final String Error;

  MFPaymentStatus(
      this.IsSuccess,
      this.InvoiceId,
      this.InvoiceStatus,
      this.InvoiceReference,
      this.InvoiceTransactions
//      this.TransactionDate,
//      this.TransactionId,
//      this.PaymentGateway,
//      this.TrackId,
//      this.PaymentId,
//      this.PaidCurrency,
//      this.PaidCurrencyValue,
//      this.Error
      );

  MFPaymentStatus.fromJson(Map<String, dynamic> json)
      : IsSuccess = json['IsSuccess'],
        InvoiceId = json['Data']['InvoiceId'].toString(),
        InvoiceStatus = json['Data']['InvoiceStatus'],
        InvoiceReference = json['Data']['InvoiceReference'],
//        InvoiceTransactions = json['Data']['InvoiceTransactions'];
        InvoiceTransactions = (json['Data']['InvoiceTransactions'] as List).map((i) => PaymentStatusInvoiceAttributes.fromJson(i)).toList();
//        TransactionDate = json['Data']['InvoiceTransactions']['TransactionDate'],
//        TransactionId = json['Data']['InvoiceTransactions']['TransactionId'],
//        PaymentGateway = json['Data']['InvoiceTransactions']['PaymentGateway'],
//        TrackId = json['Data']['InvoiceTransactions']['TrackId'],
//        PaymentId = json['Data']['InvoiceTransactions']['PaymentId'],
//        PaidCurrency = json['Data']['InvoiceTransactions']['PaidCurrency'],
//        PaidCurrencyValue = json['Data']['InvoiceTransactions']['PaidCurrencyValue'],
//        Error = json['Data']['InvoiceTransactions']['Error'];
}