class PaymentStatusInvoiceAttributes {

  final String TransactionDate;
  final String TransactionId;
  final String PaymentGateway;
  final String TrackId;
  final String PaymentId;
  final String PaidCurrency;
  final String PaidCurrencyValue;
  final String Error;

  PaymentStatusInvoiceAttributes(
      this.TransactionDate,
      this.TransactionId,
      this.PaymentGateway,
      this.TrackId,
      this.PaymentId,
      this.PaidCurrency,
      this.PaidCurrencyValue,
      this.Error
      );

  PaymentStatusInvoiceAttributes.fromJson(Map<String, dynamic> json)
      :
        TransactionDate = json['TransactionDate'],
        TransactionId = json['TransactionId'],
        PaymentGateway = json['PaymentGateway'],
        TrackId = json['TrackId'],
        PaymentId = json['PaymentId'],
        PaidCurrency = json['PaidCurrency'],
        PaidCurrencyValue = json['PaidCurrencyValue'],
        Error = json['Error'];
}