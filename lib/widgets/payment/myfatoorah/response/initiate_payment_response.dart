import '../../myfatoorah/response/payment_method.dart';
import '../../myfatoorah/response/base_response.dart';

class InitiatePaymentResponse extends MyFatoorahResponse<_InitiatePaymentResponseData> {
  InitiatePaymentResponse.fromJson(Map<String, dynamic> json)
      : super.fromJson(json);
  @override
  _InitiatePaymentResponseData mapData(Map<String, dynamic> json) {
    return _InitiatePaymentResponseData.fromJson(json);
  }
}

class _InitiatePaymentResponseData {
  List<MFPaymentMethod> paymentMethods;
  _InitiatePaymentResponseData.fromJson(Map<String, dynamic> json) {
    if (json['PaymentMethods'] != null || json['paymentMethods'] != null) {
      paymentMethods = new List<MFPaymentMethod>();
      (json['PaymentMethods'] ?? json['paymentMethods']).forEach((v) {
        paymentMethods.add(new MFPaymentMethod.fromJson(v));
      });
    }
  }
}
