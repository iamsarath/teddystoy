import 'dart:convert';
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:teddystoy/common/config.dart';
import 'package:teddystoy/common/constants.dart';
import 'package:teddystoy/common/constants/general.dart';
import 'package:teddystoy/common/tools.dart';
import 'package:teddystoy/models/app.dart';
import 'package:teddystoy/widgets/payment/myfatoorah/request/payment_status_request.dart';
import 'package:teddystoy/widgets/payment/myfatoorah/response/payment_status_invoice_attributes.dart';
import 'package:teddystoy/widgets/payment/myfatoorah/response/payment_status_response.dart';
import 'package:teddystoy/common/styles.dart';
//import 'package:teddystoy/common/tools.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class MyFatoorahTransactionDetails extends StatefulWidget {
//  final Map<String, dynamic> params;
  final String invoiceId;
  MyFatoorahTransactionDetails({this.invoiceId});

  @override
  State<StatefulWidget> createState() {
    return MyFatoorahTransactionDetailsState();
  }
}

class MyFatoorahTransactionDetailsState extends State<MyFatoorahTransactionDetails> {
  String checkoutUrl;

  @override
  void initState() {
    super.initState();

  }



  /// Build the App Theme
  ThemeData getTheme(context) {
    printLog("[AppState] build Theme");

    AppModel appModel = Provider.of<AppModel>(context);
    bool isDarkTheme = appModel.darkTheme ?? false;

    if (appModel.appConfig == null) {
      /// This case is loaded first time without config file
      return buildLightTheme(appModel.locale);
    }

    if (isDarkTheme) {
      return buildDarkTheme(appModel.locale).copyWith(
        primaryColor: HexColor(
          appModel.appConfig["Setting"]["MainColor"],
        ),
      );
    }
    return buildLightTheme(appModel.locale).copyWith(
      primaryColor: HexColor(appModel.appConfig["Setting"]["MainColor"]),
    );
  }

  Widget getTable(_response, invoiceTransactionsList){
    return
      Column(
          children: <Widget>[
            SizedBox(height: 25.0,),
          Card(
              margin: const EdgeInsets.all(15.0),
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(6.0),
              ),
              elevation: 3,
              child: ListView(
               shrinkWrap: true,
               children: <Widget>[
                 SizedBox(height: 50.0,),
                Center(
                    child: Text(
                      'You Paid : ${invoiceTransactionsList[0].PaidCurrency} ${invoiceTransactionsList[0].PaidCurrencyValue}',
                      style: Theme.of(context).textTheme.headline6.copyWith(
                        fontSize: 20,
                        color: Colors.green,
                      ),
                    )),
                DataTable(
                  columns: [
                    DataColumn(label: Text('',
                        style: TextStyle(
                            color: Colors.black, fontSize: 15,
                            fontWeight: FontWeight.bold))),
                    DataColumn(label: Text('',
                        style: TextStyle(
                            color: Colors.black, fontSize: 15,
                            fontWeight: FontWeight.bold))),
                  ],
                  rows: [
                    DataRow(cells: [
                      DataCell(Text('Invoice Id',
                        style: TextStyle(
                            color: Colors.black, fontSize: 13,
                            fontWeight: FontWeight.bold),
                      )),
                      DataCell(Text(_response.InvoiceId,
                        style: TextStyle(
                            color: Colors.black, fontSize: 13,
                            fontWeight: FontWeight.bold),
                      )),
                    ]),
                    DataRow(cells: [
                      DataCell(Text('Transaction Date',
                        style: TextStyle(
                            color: Colors.black, fontSize: 13,
                            fontWeight: FontWeight.bold),
                      )),

                      DataCell(Text(DateFormat("dd/MM/yyyy HH.mm aa").format(DateTime.parse(invoiceTransactionsList[0].TransactionDate)),
                        style: TextStyle(
                            color: Colors.black, fontSize: 13,
                            fontWeight: FontWeight.bold),
                      )),
                    ]),
                    DataRow(cells: [
                      DataCell(Text('Transaction Id',
                        style: TextStyle(
                            color: Colors.black, fontSize: 13,
                            fontWeight: FontWeight.bold),
                      )),
                      DataCell(Text(invoiceTransactionsList[0].TransactionId,
                        style: TextStyle(
                            color: Colors.black, fontSize: 13,
                          fontWeight: FontWeight.bold),
                    )),
                  ]),
                  DataRow(cells: [
                    DataCell(Text('Track Id',
                      style: TextStyle(
                          color: Colors.black, fontSize: 13,
                          fontWeight: FontWeight.bold),
                    )),
                    DataCell(Text(invoiceTransactionsList[0].TrackId,
                      style: TextStyle(
                          color: Colors.black, fontSize: 13,
                          fontWeight: FontWeight.bold),
                    )),

                  ]),

                ],
              ),
            ]),
    ),
//          ),

            const SizedBox(height: 10),

//            MaterialButton(
//            height: 40.0,
//            minWidth: 100.0,
//            color: Colors.green,
//            textColor: Colors.white,
//                child: Text(
//                'Close',
//                style: TextStyle(fontSize: 16)
//                ),
//            onPressed: () => {
//                Navigator.pop(context)
//              },
//            splashColor: ThemeColor,
//            ),
        ],
      );
      Container(
        color: Colors.white,

      );
  }

  @override
  Widget build(BuildContext context) {
    return
//      MaterialApp(
//      debugShowCheckedModeBanner: false,
//      theme: getTheme(context),
//      home:
        Scaffold(
          appBar: AppBar(
            title: Text('Transaction Details',
                style: TextStyle(
                color: Colors.white, fontSize: 18,
                fontWeight: FontWeight.bold),
                textAlign: TextAlign.center),
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              color: Colors.white,
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),

          body: FutureBuilder<MFPaymentStatus>(
            future: getPaymentStatus(widget.invoiceId, widget),
            builder: (context, snapshot) {
              printLog("snapshot.hasData = ${snapshot.data}");
              printLog("snapshot.connectionState = ${snapshot.connectionState}");

              if(snapshot.connectionState == ConnectionState.done) {
                MFPaymentStatus _response =  snapshot.data;
                if (_response.IsSuccess) {
                  List<PaymentStatusInvoiceAttributes> invoiceTransactionsList = _response.InvoiceTransactions;

                  return Container(
//                    child: showView(_response, invoiceTransactionsList),
                    child: getTable(_response, invoiceTransactionsList),
                  );

                } else {
//          throw Exception(_response.Error);
                  throw Exception("Error");
                }
              }
              else{

                return kLoadingWidget(context);
              }

            },
          ));
//    );
  }

  Future<MFPaymentStatus> getPaymentStatus(invoiceId, widget) {

    var base_url = MyFatoorahConfig["isLive"] ? MyFatoorahConfig["urlLive"] : MyFatoorahConfig["urlTest"];
    var url = '$base_url/v2/GetPaymentStatus';
    var authToken = MyFatoorahConfig["isLive"] ? MyFatoorahConfig["tokenLive"] : MyFatoorahConfig["tokenTest"];

    return http.post(url,
        body: jsonEncode(PaymentIdKey(invoiceId, "invoiceId").toJson()),
        headers: {
          "Content-Type": "application/json",
          "Authorization": "bearer ${authToken}"
        }).then((response) {
      if (response.statusCode == 200) {

        var json = jsonDecode(response.body);
        printLog("json---> ${json}");

        MFPaymentStatus _response = MFPaymentStatus.fromJson(json);
        printLog("_response.InvoiceId---> ${_response.InvoiceId}");

        if (_response.IsSuccess) {

          return _response;
        } else {
          throw Exception("Error");
        }
      } else {
        throw Exception(response.body);
      }
    });
  }
}

