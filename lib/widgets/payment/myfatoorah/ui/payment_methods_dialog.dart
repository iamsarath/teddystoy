import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:teddystoy/common/constants.dart';
import 'package:teddystoy/models/cart/cart_base.dart';
import 'package:teddystoy/models/payment_method.dart';
import 'package:teddystoy/screens/checkout/single_page_checkout/order_success.dart';
import 'package:teddystoy/services/helper/magento.dart';
import 'package:teddystoy/services/index.dart';
import 'package:teddystoy/widgets/payment/myfatoorah/response/payment_status_invoice_attributes.dart';
import 'package:teddystoy/widgets/payment/myfatoorah/response/payment_status_response.dart';
import 'package:teddystoy/widgets/payment/myfatoorah/ui/transaction_details.dart';
import '../../myfatoorah/request/payment_status_request.dart';
import '../../myfatoorah/request/my_fatoorah_request.dart';
import '../../myfatoorah/response/payment_method.dart';
import '../../myfatoorah/ui/payment_method.dart';
import '../../myfatoorah/ui/web_view_page.dart';
import '../../myfatoorah/response/payment_response.dart';
import '../../myfatoorah/enums/other.dart';
import '../../myfatoorah/response/initiate_payment_response.dart';
import 'package:teddystoy/screens/checkout/single_page_checkout/single_checkout_payment.dart';

class PaymentMethodsBuilder extends StatefulWidget {
  final MyfatoorahRequest request;
  final String selectedId;
  final Widget widgetOrderPage;
  final dialogContext;
  final mainContext;
  final paymentScreenContext;
  final Function(PaymentResponse res) onResult;
  final Widget Function(MFPaymentMethod method, bool loading, String error)
      buildPaymentMethod;
  final Widget Function(List<Widget> methods) paymentMethodsBuilder;
  const PaymentMethodsBuilder({
    Key key,
    this.request,
    this.buildPaymentMethod,
    this.paymentMethodsBuilder,
    this.onResult,
    this.selectedId,
    this.widgetOrderPage,
    this.dialogContext,
    this.mainContext,
    this.paymentScreenContext
  }) : super(key: key);
  @override
  PaymentMethodsBuilderState createState() => PaymentMethodsBuilderState();
}

class PaymentMethodsBuilderState extends State<PaymentMethodsBuilder>
    with TickerProviderStateMixin {
  List<MFPaymentMethod> methods = [];
  bool loading = true;
  String errorMessage;
  String url;
  FlutterWebviewPlugin flutterWebviewPlugin = FlutterWebviewPlugin();
  final Services _service = Services();

  Future loadMethods() {
    var url = widget.request.initiatePaymentUrl ??
        '${widget.request.url}/v2/InitiatePayment';
    return http.post(url,
        body: jsonEncode(widget.request.intiatePaymentRequest()),
        headers: {
          "Content-Type": "application/json",
          "Authorization": "bearer ${widget.request.authorizationToken}",
        }).then((response) {
      if (response.statusCode == 200) {

//        Navigator.push(
//          context,
//          MaterialPageRoute(builder: (context) => OrderSuccess()),
//        );

        printLog("Authorization token---> ${widget.request.authorizationToken}");
        var json = jsonDecode(response.body);
        var _response = InitiatePaymentResponse.fromJson(json);
        setState(() {
          methods = _response.isSuccess ? _response.data.paymentMethods : null;
          errorMessage = _response.isSuccess ? null : _response.message;
          loading = false;
        });
      } else {
        setState(() {
          loading = false;
          errorMessage = response.body;
        });
      }
    }).catchError((e) {
      print(e);
      setState(() {
        loading = false;
        errorMessage = e.toString();
      });
    });
  }

  PaymentResponse getResponse() {
    if (url == null) return PaymentResponse(PaymentStatus.None);
    Uri uri = Uri.parse(url);
    var isSuccess = url.contains(widget.request.successUrl);
    var isError = url.contains(widget.request.errorUrl);
    if (!isError && !isSuccess) return PaymentResponse(PaymentStatus.None);
    PaymentStatus status =
        isSuccess ? PaymentStatus.Success : PaymentStatus.Error;

    return PaymentResponse(status, uri.queryParameters["paymentId"]);
  }

  void placeOrder(selectedId, widgetOrderPage, paymentId){

    final paymentMethodModel = Provider.of<PaymentMethodModel>(context, listen: false);
    final paymentMethod = paymentMethodModel.paymentMethods.firstWhere((item) => item.id == selectedId);

    Provider.of<CartModel>(context, listen: false)
        .setPaymentMethod(paymentMethod);

    final cartModel = Provider.of<CartModel>(context, listen: false);

    printLog("selectedId---> ${selectedId}");
    printLog("paymentId---> ${paymentId}");
    printLog("cartModel.toString()---> ${cartModel.toString()}");

    Services().widget.placeOrderFromMyFatoorah(
      context,
      cartModel: cartModel,
      onLoading: widgetOrderPage.onLoading,
      paymentMethod: paymentMethod,
      success: (order) {
        widgetOrderPage.onFinish(order);
        widgetOrderPage.onLoading(false);

        updateTransactionDetails(order : order, paymentId: paymentId);
      },
      error: (message) {
        widgetOrderPage.onLoading(false);
        final snackBar = SnackBar(
          content: Text(message),
        );
//        Scaffold.of(context).showSnackBar(snackBar);
        printLog("Error in placeOrder --->   $message");
        printLog("line 144 payment web closing 5--->  closing $context");
        Navigator.of(context).pop();
      },
    );
  }

  Future<void> updateTransactionDetails({order, paymentId}) async {
    try {
      bool isSuccess = await _service.updateMyFatoorahTransactionDetails(context: context, orderId: "${order.id}", paymentId: paymentId);

      printLog("response---> ${isSuccess}");
      printLog("updateTransactionDetails");

      if (isSuccess) {

        printLog("order---> ${order}");
        printLog("context---> ${context}");
        printLog("mainContext---> ${widget.mainContext}");
        printLog("dialogContext---> ${widget.dialogContext}");
        printLog("paymentScreenContext---> ${widget.paymentScreenContext}");

        Navigator.of(widget.mainContext).pop();
//        Navigator.of(context).pop(order);
      }
      else{

        printLog("line 184 payment web closing 4--->  closing $context");
        Navigator.of(context).pop();
      }
    } catch (e) {
      rethrow;
    }
  }

//    Future<void> createOrder({paid = false, cod = false, widgetPlaceOrder}) async {
//      widgetPlaceOrder.onLoading(true);
//      await Services().widget.createOrder(
//        context,
//        paid: paid,
//        cod: cod,
//        success: (order) {
//          widgetPlaceOrder.onFinish(order);
//          widgetPlaceOrder.onLoading(false);
//          Navigator.of(context).pop();
//        },
//        error: (message) {
//          widgetPlaceOrder.onLoading(false);
//          printLog("Error message--> ${message}");
////          final snackBar = SnackBar(
////            content: Text(message),
////          );
////          Scaffold.of(context).showSnackBar(snackBar);
//        },
//      );
//    }

  @override
  void initState() {
    loadMethods();

    flutterWebviewPlugin.onStateChanged.listen((state) {
      url = state.url;
      printLog("url--> ${url}");
      printLog("state.type--> ${state.type}");
      if (state.type == WebViewState.shouldStart) {
        if (widget.request.afterPaymentBehaviour ==
            AfterPaymentBehaviour.BeforeCalbacksExecution) {
          var response = getResponse();

          if (response.status != PaymentStatus.None) {
            printLog("line 237 payment web closing 2--->  closing $context");
            Navigator.of(context).pop();
            return;
          }
        }
      } else if (state.type == WebViewState.finishLoad) {
        if (widget.request.afterPaymentBehaviour ==
            AfterPaymentBehaviour.AfterCalbacksExecution) {
          var response = getResponse();
          printLog("url response--> ${response}");
          if (response.status != PaymentStatus.None) {
            printLog("line 237 payment web closing 1--->  closing $context");
            Navigator.of(context).pop();
            return;
          }
        }
        else{
          var response = getResponse();
          printLog("119 url response--> ${response}");
          printLog("response.paymentId--> ${response.paymentId}");
          if (response.status == PaymentStatus.Success) {

            if (response.paymentId != null) {

//              Navigator.push(
//                context,
//                MaterialPageRoute(builder: (context) => OrderSuccess()),
//              );
//              ,,,
              placeOrder(widget.selectedId, widget.widgetOrderPage, response.paymentId);
            }

            return;
          }
          else{
            if (response.paymentId != null) {
              Future.delayed(Duration(seconds: 7), () {
                // 5s over, navigate to a new page
                printLog("line 268 payment error--->  closing $context");
                Navigator.of(context).pop();
              });
            }
          }
        }
      }
    });
    super.initState();
  }

  @override
  dispose() {
    flutterWebviewPlugin.close();
    flutterWebviewPlugin.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (widget.onResult != null) return true;
        var response = getResponse();
        Navigator.of(context).pop(response);
        return false;
      },
      child: AnimatedSize(
        vsync: this,
        duration: Duration(milliseconds: 300),
        child: buildChild(),
      ),
    );
  }

  Widget buildChild() {
    if (loading == true) {
      return kLoadingWidget(context);
    } else if (errorMessage != null) {
      return buildError();
    } else {
      List<Widget> childs = methods.map((e) {
        return PaymentMethodItem(
          method: e.withLangauge(widget.request.language),
          request: widget.request,
          buildPaymentMethod: widget.buildPaymentMethod,
          onLaunch: (String _url) {

            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => WebViewPage(url: _url),
              ),
            ).then((value) {
              var response = getResponse();
              if (response.status != null &&
                  response.status != PaymentStatus.None) {
                if (widget.onResult != null)
                  widget.onResult(response);
                else
                  Navigator.of(context).pop(response);
              }
            });


          },
        );
      }).toList();
      if (widget.paymentMethodsBuilder != null)
        return widget.paymentMethodsBuilder(childs);
      return ListView(
        shrinkWrap: true,
        children: ListTile.divideTiles(
          color: Colors.black26,
          tiles: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 30),
              child: Text(
                widget.request.invoiceAmount.toStringAsFixed(2),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            ...childs,
          ],
        ).toList(),
      );
    }
  }

  Widget buildError() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.warning),
            iconSize: 50,
            onPressed: () {
              setState(() {
                loading = true;
              });
              loadMethods();
            },
          ),
          SizedBox(height: 15),
          Text(
            errorMessage,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  SizedBox buildLoading() {
    return SizedBox(
      height: 100,
      child: Center(
        child: SpinKitFadingCube(
          color: Theme.of(context).primaryColor,
          size: 30.0,
        ),
      ),
    );
  }
}
