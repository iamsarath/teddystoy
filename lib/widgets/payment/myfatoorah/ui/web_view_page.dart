import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:teddystoy/common/constants.dart';

class WebViewPage extends StatefulWidget {
  final String url;

  const WebViewPage({Key key, this.url}) : super(key: key);
  @override
  __WebViewPageState createState() => __WebViewPageState();
}

class __WebViewPageState extends State<WebViewPage> {
  FlutterWebviewPlugin flutterWebviewPlugin = FlutterWebviewPlugin();
  bool loading = true;
  @override
  void initState() {
    flutterWebviewPlugin.onStateChanged.listen((state) {
      print("\u001b[31mWeView=> ${state.type}");
      if (state.type == WebViewState.shouldStart ||
          state.type == WebViewState.startLoad) {
        setState(() {
          loading = true;
        });
      } else {
        setState(() {
          loading = false;
        });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    printLog("-----------> Finishing webview");
  }
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('Proceed to pay',
            style: TextStyle(
                color: Colors.white, fontSize: 18,
                fontWeight: FontWeight.bold),
            textAlign: TextAlign.center),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
          onPressed: () {
//            Navigator.pop(context);
            Fluttertoast.showToast(
                msg: "Please don't press back button",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIosWeb: 1,
                backgroundColor: ThemeColor,
                textColor: Colors.white,
                fontSize: 16.0
            );
          },
        ),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(5),
          child: AnimatedOpacity(
            opacity: loading ? 1 : 0,
            duration: Duration(milliseconds: 300),
            child: LinearProgressIndicator(
              backgroundColor: Colors.green,
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.amber),
            ),
          ),
        ),
      ),
      url: widget.url,
      withJavascript: true,
      useWideViewPort: true,
      withZoom: true,
      hidden: true,
      ignoreSSLErrors: true,
    );
  }
}
