//import 'package:flutter/material.dart';
//import 'package:myfatoorah_flutter/myfatoorah_flutter.dart';
//
///*
//TODO: The following data are using for testing only, so that when you go live
//      don't forget to replace the following test credentials with the live
//      credentials provided by MyFatoorah Company.
//*/
//
//// Base Url
//final String baseUrl = "https://apitest.myfatoorah.com";
//
//// Token for regular payment
//final String regularPaymentToken =
//    "cxu2LdP0p0j5BGna0velN9DmzKJTrx3Ftc0ptV8FmvOgoDqvXivkxZ_oqbi_XM9k7jgl3SUriQyRE2uaLWdRumxDLKTn1iNglbQLrZyOkmkD6cjtpAsk1_ctrea_MeOQCMavsQEJ4EZHnP4HoRDOTVRGvYQueYZZvVjsaOLOubLkdovx6STu9imI1zf5OvuC9rB8p0PNIR90rQ0-ILLYbaDZBoQANGND10HdF7zM4qnYFF1wfZ_HgQipC5A7jdrzOoIoFBTCyMz4ZuPPPyXtb30IfNp47LucQKUfF1ySU7Wy_df0O73LVnyV8mpkzzonCJHSYPaum9HzbvY5pvCZxPYw39WGo8pOMPUgEugtaqepILwtGKbIJR3_T5Iimm_oyOoOJFOtTukb_-jGMTLMZWB3vpRI3C08itm7ealISVZb7M3OMPPXgcss9_gFvwYND0Q3zJRPmDASg5NxRlEDHWRnlwNKqcd6nW4JJddffaX8p-ezWB8qAlimoKTTBJCe5CnjT4vNjnWlJWscvk38VNIIslv4gYpC09OLWn4rDNeoUaGXi5kONdEQ0vQcRjENOPAavP7HXtW1-Vz83jMlU3lDOoZsdEKZReNYpvdFrGJ5c3aJB18eLiPX6mI4zxjHCZH25ixDCHzo-nmgs_VTrOL7Zz6K7w6fuu_eBK9P0BDr2fpS";
//
//// Token for direct payment and recurring
//final String directPaymentToken =
//    "fVysyHHk25iQP4clu6_wb9qjV3kEq_DTc1LBVvIwL9kXo9ncZhB8iuAMqUHsw-vRyxr3_jcq5-bFy8IN-C1YlEVCe5TR2iCju75AeO-aSm1ymhs3NQPSQuh6gweBUlm0nhiACCBZT09XIXi1rX30No0T4eHWPMLo8gDfCwhwkbLlqxBHtS26Yb-9sx2WxHH-2imFsVHKXO0axxCNjTbo4xAHNyScC9GyroSnoz9Jm9iueC16ecWPjs4XrEoVROfk335mS33PJh7ZteJv9OXYvHnsGDL58NXM8lT7fqyGpQ8KKnfDIGx-R_t9Q9285_A4yL0J9lWKj_7x3NAhXvBvmrOclWvKaiI0_scPtISDuZLjLGls7x9WWtnpyQPNJSoN7lmQuouqa2uCrZRlveChQYTJmOr0OP4JNd58dtS8ar_8rSqEPChQtukEZGO3urUfMVughCd9kcwx5CtUg2EpeP878SWIUdXPEYDL1eaRDw-xF5yPUz-G0IaLH5oVCTpfC0HKxW-nGhp3XudBf3Tc7FFq4gOeiHDDfS_I8q2vUEqHI1NviZY_ts7M97tN2rdt1yhxwMSQiXRmSQterwZWiICuQ64PQjj3z40uQF-VHZC38QG0BVtl-bkn0P3IjPTsTsl7WBaaOSilp4Qhe12T0SRnv8abXcRwW3_HyVnuxQly_OsZzZry4ElxuXCSfFP2b4D2-Q";
//
//class MyFatoorahHome extends StatelessWidget {
//  // This widget is the root of your application.
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      child: MyFatoorahPayments(),
//    );
//  }
//}
//
//class MyFatoorahPayments extends StatefulWidget {
//  MyFatoorahPayments({Key key, this.title}) : super(key: key);
//
//  final String title;
//
//  @override
//  _MyFatoorahPayments createState() => _MyFatoorahPayments();
//}
//
//class _MyFatoorahPayments extends State<MyFatoorahPayments> {
//  String _response = '';
//  String _loading = "Loading...";
//
//  List<PaymentMethods> paymentMethods = List();
//  List<bool> isSelected = List();
//  int selectedPaymentMethodIndex = -1;
//
//  String amount = "0.100";
//  String cardNumber = "2223000000000007";
//  String expiryMonth = "5";
//  String expiryYear = "21";
//  String securityCode = "100";
//  bool visibilityObs = false;
//
//  @override
//  void initState() {
//    super.initState();
//
//    // TODO, don't forget to init the MyFatoorah Plugin with the following line
//    MFSDK.init(baseUrl, directPaymentToken);
//
//    // (Optional) un comment the following lines if you want to set up properties of AppBar.
//
////    MFSDK.setUpAppBar(
////      title: "MyFatoorah Payment",
////      titleColor: Colors.white,  // Color(0xFFFFFFFF)
////      backgroundColor: Colors.black, // Color(0xFF000000)
////      isShowAppBar: true); // For Android platform only
//
//    // (Optional) un comment this line, if you want to hide the AppBar.
//    // Note, if the platform is iOS, this line will not affected
//
////    MFSDK.setUpAppBar(isShowAppBar: false);
//
//    initiatePayment();
//  }
//
//  /*
//    Send Payment
//   */
//  void sendPayment() {
//    var request = MFSendPaymentRequest(
//        invoiceValue: double.parse(amount),
//        customerName: "Customer name",
//        notificationOption: MFNotificationOption.LINK);
//
//    MFSDK.sendPayment(
//        MFAPILanguage.EN,
//        request,
//            (MFResult<MFSendPaymentResponse> result) => {
//          if (result.isSuccess())
//            {
//              setState(() {
//                print(result.response.toJson());
//                _response = result.response.toJson().toString();
//              })
//            }
//          else
//            {
//              setState(() {
//                print(result.error.toJson());
//                _response = result.error.message;
//              })
//            }
//        });
//
//    setState(() {
//      _response = _loading;
//    });
//  }
//
//  /*
//    Initiate Payment
//   */
//  void initiatePayment() {
//    var request = new MFInitiatePaymentRequest(
//        double.parse(amount), MFCurrencyISO.KUWAIT_KWD);
//
//    MFSDK.initiatePayment(
//        request,
//        MFAPILanguage.EN,
//            (MFResult<MFInitiatePaymentResponse> result) => {
//          if (result.isSuccess())
//            {
//              setState(() {
//                print(result.response.toJson());
//                _response = ""; //result.response.toJson().toString();
//                paymentMethods.addAll(result.response.paymentMethods);
//                for (int i = 0; i < paymentMethods.length; i++)
//                  isSelected.add(false);
//              })
//            }
//          else
//            {
//              setState(() {
//                print(result.error.toJson());
//                _response = result.error.message;
//              })
//            }
//        });
//
//    setState(() {
//      _response = _loading;
//    });
//  }
//
//  /*
//    Execute Regular Payment
//   */
//  void executeRegularPayment(String paymentMethodId) {
//    var request = new MFExecutePaymentRequest(paymentMethodId, amount);
//
//    MFSDK.executePayment(
//        context,
//        request,
//        MFAPILanguage.EN,
//            (String invoiceId, MFResult<MFPaymentStatusResponse> result) => {
//          if (result.isSuccess())
//            {
//              setState(() {
//                print(invoiceId);
//                print(result.response.toJson());
//                _response = result.response.toJson().toString();
//              })
//            }
//          else
//            {
//              setState(() {
//                print(invoiceId);
//                print(result.error.toJson());
//                _response = result.error.message;
//              })
//            }
//        });
//
//    setState(() {
//      _response = _loading;
//    });
//  }
//
//  /*
//    Execute Direct Payment
//   */
//  void executeDirectPayment(String paymentMethodId) {
//    var request = new MFExecutePaymentRequest(paymentMethodId, amount);
//
////    var mfCardInfo = new MFCardInfo(cardToken: "Put your token here");
//
//    var mfCardInfo = new MFCardInfo(
//        cardNumber: cardNumber,
//        expiryMonth: expiryMonth,
//        expiryYear: expiryYear,
//        securityCode: securityCode,
//        bypass3DS: true,
//        saveToken: false);
//
//    MFSDK.executeDirectPayment(
//        context,
//        request,
//        mfCardInfo,
//        MFAPILanguage.EN,
//            (String invoiceId, MFResult<MFDirectPaymentResponse> result) => {
//          if (result.isSuccess())
//            {
//              setState(() {
//                print(invoiceId);
//                print(result.response.toJson());
//                _response = result.response.toJson().toString();
//              })
//            }
//          else
//            {
//              setState(() {
//                print(invoiceId);
//                print(result.error.toJson());
//                _response = result.error.message;
//              })
//            }
//        });
//
//    setState(() {
//      _response = _loading;
//    });
//  }
//
//  /*
//    Execute Direct Payment with Recurring
//   */
//  void executeDirectPaymentWithRecurring() {
//    // The value "2" is the paymentMethodId of Visa/Master payment method.
//    // You should call the "initiatePayment" API to can get this id and the ids of all other payment methods
//    String paymentMethod = "2";
//
//    var request = new MFExecutePaymentRequest(paymentMethod, amount);
//
//    var mfCardInfo = new MFCardInfo(
//        cardNumber: cardNumber,
//        expiryMonth: expiryMonth,
//        expiryYear: expiryYear,
//        securityCode: securityCode,
//        bypass3DS: true,
//        saveToken: true);
//
//    int intervalDays = 5;
//
//    MFSDK.executeDirectPaymentWithRecurring(
//        context,
//        request,
//        mfCardInfo,
//        intervalDays,
//        MFAPILanguage.EN,
//            (String invoiceId, MFResult<MFDirectPaymentResponse> result) => {
//          if (result.isSuccess())
//            {
//              setState(() {
//                print(invoiceId);
//                print(result.response.toJson());
//                _response = result.response.toJson().toString();
//              })
//            }
//          else
//            {
//              setState(() {
//                print(invoiceId);
//                print(result.error.toJson());
//                _response = result.error.message;
//              })
//            }
//        });
//
//    setState(() {
//      _response = _loading;
//    });
//  }
//
//  /*
//    Payment Enquiry
//   */
//  void getPaymentStatus() {
//    var request = MFPaymentStatusRequest(invoiceId: "12345");
//
//    MFSDK.getPaymentStatus(
//        MFAPILanguage.EN,
//        request,
//            (MFResult<MFPaymentStatusResponse> result) => {
//          if (result.isSuccess())
//            {
//              setState(() {
//                print(result.response.toJson());
//                _response = result.response.toJson().toString();
//              })
//            }
//          else
//            {
//              setState(() {
//                print(result.error.toJson());
//                _response = result.error.message;
//              })
//            }
//        });
//
//    setState(() {
//      _response = _loading;
//    });
//  }
//
//  /*
//    Cancel Token
//   */
//  void cancelToken() {
//    MFSDK.cancelToken(
//        "Put your token here",
//        MFAPILanguage.EN,
//            (MFResult<bool> result) => {
//          if (result.isSuccess())
//            {
//              setState(() {
//                print(result.response.toString());
//                _response = result.response.toString();
//              })
//            }
//          else
//            {
//              setState(() {
//                print(result.error.toJson());
//                _response = result.error.message;
//              })
//            }
//        });
//
//    setState(() {
//      _response = _loading;
//    });
//  }
//
//  /*
//    Cancel Recurring Payment
//   */
//  void cancelRecurringPayment() {
//    MFSDK.cancelRecurringPayment(
//        "Put RecurringId here",
//        MFAPILanguage.EN,
//            (MFResult<bool> result) => {
//          if (result.isSuccess())
//            {
//              setState(() {
//                print(result.response.toString());
//                _response = result.response.toString();
//              })
//            }
//          else
//            {
//              setState(() {
//                print(result.error.toJson());
//                _response = result.error.message;
//              })
//            }
//        });
//
//    setState(() {
//      _response = _loading;
//    });
//  }
//
//  void setPaymentMethodSelected(int index, bool value) {
//    for (int i = 0; i < isSelected.length; i++) {
//      if (i == index) {
//        isSelected[i] = value;
//        if (value) {
//          selectedPaymentMethodIndex = index;
//          visibilityObs = paymentMethods[index].isDirectPayment;
//        } else {
//          selectedPaymentMethodIndex = -1;
//          visibilityObs = false;
//        }
//      } else
//        isSelected[i] = false;
//    }
//  }
//
//  void pay() {
//    if (selectedPaymentMethodIndex == -1) {
//
//      final snackBar = SnackBar(
//        content: Text("Please select payment method first"),
//      );
//      Scaffold.of(context).showSnackBar(snackBar);
//    } else {
//      if (amount.isEmpty) {
//        final snackBar = SnackBar(
//          content: Text("Set the amount"),
//        );
//
//      } else if (paymentMethods[selectedPaymentMethodIndex].isDirectPayment) {
//        if (cardNumber.isEmpty ||
//            expiryMonth.isEmpty ||
//            expiryYear.isEmpty ||
//            securityCode.isEmpty) {
//          final snackBar = SnackBar(
//            content: Text("Fill all the card fields"),
//          );
//          Scaffold.of(context).showSnackBar(snackBar);
//        }
//        else {
//          executeDirectPayment(paymentMethods[selectedPaymentMethodIndex]
//              .paymentMethodId
//              .toString());
//        }
//      } else
//        executeRegularPayment(paymentMethods[selectedPaymentMethodIndex]
//            .paymentMethodId
//            .toString());
//    }
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      body: SafeArea(
//        child: Container(
//          margin: EdgeInsets.all(10.0),
//          child: Column(children: [
//            TextField(
//              keyboardType: TextInputType.number,
//              controller: TextEditingController(text: amount),
//              decoration: InputDecoration(labelText: "Payment Amount"),
//              onChanged: (value) {
//                amount = value;
//              },
//            ),
//            Padding(
//              padding: EdgeInsets.all(5.0),
//            ),
//            Text("Select payment method"),
//            Padding(
//              padding: EdgeInsets.all(5.0),
//            ),
//            Expanded(
//                child: GridView.builder(
//                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//                        crossAxisCount: 4,
//                        crossAxisSpacing: 0.0,
//                        mainAxisSpacing: 0.0),
//                    itemCount: paymentMethods.length,
//                    itemBuilder: (BuildContext ctxt, int index) {
//                      return Column(
//                        children: <Widget>[
//                          Image.network(paymentMethods[index].imageUrl,
//                              width: 40.0, height: 40.0),
//                          Checkbox(
//                              value: isSelected[index],
//                              onChanged: (bool value) {
//                                setState(() {
//                                  setPaymentMethodSelected(index, value);
//                                });
//                              })
//                        ],
//                      );
//                    })),
//            visibilityObs
//                ? Container(
//                child: Column(
//                  children: <Widget>[
//                    Padding(
//                      padding: EdgeInsets.all(5.0),
//                    ),
//                    TextField(
//                      keyboardType: TextInputType.number,
//                      decoration: InputDecoration(labelText: "Card Number"),
//                      controller: TextEditingController(text: cardNumber),
//                      onChanged: (value) {
//                        cardNumber = value;
//                      },
//                    ),
//                    TextField(
//                      keyboardType: TextInputType.number,
//                      decoration: InputDecoration(labelText: "Expiry Month"),
//                      controller: TextEditingController(text: expiryMonth),
//                      onChanged: (value) {
//                        expiryMonth = value;
//                      },
//                    ),
//                    TextField(
//                      keyboardType: TextInputType.number,
//                      decoration: InputDecoration(labelText: "Expiry Year"),
//                      controller: TextEditingController(text: expiryYear),
//                      onChanged: (value) {
//                        expiryYear = value;
//                      },
//                    ),
//                    TextField(
//                      keyboardType: TextInputType.number,
//                      decoration: InputDecoration(labelText: "Security Code"),
//                      controller: TextEditingController(text: securityCode),
//                      onChanged: (value) {
//                        securityCode = value;
//                      },
//                    ),
//                  ],
//                ))
//                : Container(),
//            Column(
//              mainAxisAlignment: MainAxisAlignment.center,
//              crossAxisAlignment: CrossAxisAlignment.stretch,
//              children: [
//                Padding(
//                  padding: EdgeInsets.all(5.0),
//                ),
//                RaisedButton(
//                  color: Colors.lightBlue,
//                  textColor: Colors.white,
//                  child: Text('Pay'),
//                  onPressed: pay,
//                ),
//                RaisedButton(
//                  color: Colors.lightBlue,
//                  textColor: Colors.white,
//                  child: Text('Send Payment'),
//                  onPressed: sendPayment,
//                ),
//                Padding(
//                  padding: EdgeInsets.all(8.0),
//                ),
//              ],
//            ),
//            Container(
//              child: Expanded(
//                flex: 1,
//                child: SingleChildScrollView(
//                  child: Text(_response),
//                ),
//              ),
//            ),
//          ]),
//        ),
//      ),
//    );
//  }
//}
//
//class HomeScreen extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: Text('Returning Data Demo'),
//      ),
//      body: Center(child: SelectionButton()),
//    );
//  }
//}
//
//class SelectionButton extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return RaisedButton(
//      onPressed: () {
//        _navigateAndDisplaySelection(context);
//      },
//      child: Text('Pick an option, any option!'),
//    );
//  }
//
//  // A method that launches the SelectionScreen and awaits the result from
//  // Navigator.pop.
//  _navigateAndDisplaySelection(BuildContext context) async {
//    // Navigator.push returns a Future that completes after calling
//    // Navigator.pop on the Selection Screen.
//    final result = await Navigator.push(
//      context,
//      MaterialPageRoute(builder: (context) => SelectionScreen()),
//    );
//
//    // After the Selection Screen returns a result, hide any previous snackbars
//    // and show the new result.
//    Scaffold.of(context)
//      ..removeCurrentSnackBar()
//      ..showSnackBar(SnackBar(content: Text("$result")));
//  }
//}
//
//class SelectionScreen extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: Text('Pick an option'),
//      ),
//      body: Center(
//        child: Column(
//          mainAxisAlignment: MainAxisAlignment.center,
//          children: <Widget>[
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: RaisedButton(
//                onPressed: () {
//                  // Pop here with "Yep"...
//                },
//                child: Text('Yep!'),
//              ),
//            ),
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: RaisedButton(
//                onPressed: () {
//                  // Pop here with "Nope"
//                },
//                child: Text('Nope.'),
//              ),
//            )
//          ],
//        ),
//      ),
//    );
//  }
//}
//
