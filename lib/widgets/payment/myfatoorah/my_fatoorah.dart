import 'package:flutter/material.dart';
import 'package:teddystoy/screens/checkout/single_page_checkout/order_success.dart';

import './request/my_fatoorah_request.dart';
import './response/payment_method.dart';
import './response/payment_response.dart';
import './ui/payment_methods_dialog.dart';

class MyFatoorah extends StatelessWidget {

  static Future<PaymentResponse> startPayment({
    @required BuildContext context,

    /// use this to customize the single payment method
    /// the default is `ListTile`
    Widget Function(MFPaymentMethod method, bool loading, String error)
        buildPaymentMethod,

    /// user this to customize the wrapper of paymentmethods
    /// the default is `ListView`
    Widget Function(List<Widget> methods) methodsBuilder,
    @required MyfatoorahRequest request,

    String selectedId,

    Widget widgetOrderPage,
    final paymentScreenContext,

  }) {
    return showDialog(
      context: context,
      builder: (ctx) {
        return Dialog(
          child: PaymentMethodsBuilder(
            request: request,
            buildPaymentMethod: buildPaymentMethod,
            paymentMethodsBuilder: methodsBuilder,
            selectedId: selectedId,
            widgetOrderPage: widgetOrderPage,
            dialogContext: ctx,
            mainContext: context,
            paymentScreenContext: paymentScreenContext
          ),
        );
      },
    ).then((res) {
      if (res is PaymentResponse) {
        print("res---> ${res.status}");

        if(res.status.toString()=="PaymentStatus.Success"){

          print("success-->");
          Route route = MaterialPageRoute(
              builder: (context) => OrderSuccess());
          Navigator.pushReplacement(context, route);
        }
        else{
          print("err-->");
        }

        return res;
      }
      else {

        throw Exception("The payment is not completed");
      }
    });
  }

  final Widget Function(MFPaymentMethod method, bool loading, String error)
      buildPaymentMethod;

  /// use this to customize the wrapper of paymentmethods
  /// the default is `ListView`
  final Widget Function(List<Widget> methods) methodsBuilder;

  final MyfatoorahRequest request;
  final Function(PaymentResponse res) onResult;
        const MyFatoorah({
          Key key,
          this.buildPaymentMethod,
          this.methodsBuilder,
          @required this.request,
          @required this.onResult,
        }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PaymentMethodsBuilder(
      request: request,
      onResult: onResult,
      buildPaymentMethod: buildPaymentMethod,
      paymentMethodsBuilder: methodsBuilder,
    );
  }
}
