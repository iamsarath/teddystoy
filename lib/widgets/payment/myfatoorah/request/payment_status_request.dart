class PaymentIdKey {
  final String key;
  final String keyType;

  PaymentIdKey(this.key, this.keyType);

  PaymentIdKey.fromJson(Map<String, dynamic> json)
      : key = json['Key'],
        keyType = json['KeyType'];

  Map<String, dynamic> toJson() =>
      {
        'Key': key,
        'KeyType': keyType,
      };
}