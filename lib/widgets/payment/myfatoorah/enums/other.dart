

enum AfterPaymentBehaviour {
  BeforeCalbacksExecution,
  AfterCalbacksExecution,
  None,
}

enum PaymentStatus { Success, Error, None }
