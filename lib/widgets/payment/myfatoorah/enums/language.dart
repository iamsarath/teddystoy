

enum ApiLanguage { Arabic, English }

Map<ApiLanguage, String> languages = {
  ApiLanguage.Arabic: "AR",
  ApiLanguage.English: "EN",
};
