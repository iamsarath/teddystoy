import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:teddystoy/common/constants.dart';

class CircleButton extends StatelessWidget {
  final GestureTapCallback onTap;
  final IconData iconData;

  const CircleButton({Key key, this.onTap, this.iconData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double size = 36.0;

    return InkResponse(
      onTap: onTap,
      child: Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
          color: Colors.black12,
          shape: BoxShape.circle,
        ),
        child: Icon(
          iconData,
          size: 19.0,
          color: ThemeColor,
        ),
      ),
    );
  }
}
