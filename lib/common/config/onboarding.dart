/// the welcome screen data
/// set onBoardingData = [] if you would like to hide the onboarding
List onBoardingData = [
  {
    "title": "Mr Teddy's Toy House",
    "image": "assets/images/fogg-delivery-1.png",
    "desc": "Welcomes you to the world of toys !!"
  },
  {
    "title": "Fun Unlimitted",
    "image": "assets/images/fogg-uploading-1.png",
    "desc":
        "Get in touch with worlds latest puzzles and toys "
            "Fast, convenient and ease."
  },
  {
    "title": "Let's Get Started",
    "image": "assets/images/fogg-order-completed.png",
    "desc": "Nothing to wait...!, Let's start explore...!"
  },
];
