/**
 * Everything Config about the Payment
 */

/// config for payment features
const kPaymentConfig = {
  "DefaultCountryISOCode": "KW",

  /// Enable the Shipping option from Checkout, support for the Digital Download
  "EnableShipping": true,

  /// Enable the Google Map picker
  "EnableAddress": true,

  /// Enable the product review option
  "EnableReview": false,

  /// enable the google map picker from Billing Address
  'allowSearchingAddress': true,
  "GuestCheckout": true,

  /// Enable Payment option
  "EnableOnePageCheckout": true,
  "NativeOnePageCheckout": true,

  /// Enable update order status to processing after checkout by COD on woo commerce
  "UpdateOrderStatus": false
};

const Payments = {
  "paypal": "assets/icons/payment/paypal.png",
  "stripe": "assets/icons/payment/stripe.png",
  "razorpay": "assets/icons/payment/razorpay.png",
};

const PaypalConfig = {
  "clientId":
      "ASlpjFreiGp3gggRKo6YzXMyGM6-NwndBAQ707k6z3-WkSSMTPDfEFmNmky6dBX00lik8wKdToWiJj5w",
  "secret":
      "ECbFREri7NFj64FI_9WzS6A0Az2DqNLrVokBo0ZBu4enHZKMKOvX45v9Y1NBPKFr6QJv2KaSp5vk5A1G",
  "production": false,
  "paymentMethodId": "paypal",
  "enabled": true,
  "returnUrl": "http://return.example.com",
  "cancelUrl": "http://cancel.example.com",
};

const RazorpayConfig = {
  "keyId": "rzp_test_Iz3ByJRZoHMgxr",
  "paymentMethodId": "razorpay",
  "enabled": true
};

const TapConfig = {
  "SecretKey": "sk_test_XKokBfNWv6FIYuTMg5sLPjhJ",
  "RedirectUrl": "http://your_website.com/redirect_url",
  "paymentMethodId": "",
  "enabled": false
};

const MyFatoorahConfig = {
  "isLive":false,
  "urlTest": "https://apitest.myfatoorah.com",
  "urlLive": "https://api.myfatoorah.com",
  "tokenLive" : "9PTtnD4qbTf4znL4J3oMAo7RF2Y6ACfnxhrplcy1l1KOUSntEg64Ui4ecO7zWAUBGJQQqrQwmTjn9MGbWcwBSX9fw710b3n_so9A38tie256VHelDpLq22D1sOva3RXCUbcyEz6bisHDpe08S59JAw_E0CNRaQDIHfQrZN8WykcpW1JTAdFtGf_daxMv1lKTFQtutMj9mtLpGDrIEd2cVkzH9E-_qTWlAVE33JU3dHIPtg7E7sNd6EfSyOe94a_p-qVzLZGBvHWXEwTq_m5hsxDfRmp6V6ZXBb6rzm7XNp3ZPafGu2nJcNr81L_JyVhmM1PQfVt1henBz6lkShPi-sb3S1YR2KfO1TPxckAfYVmrVPv2heYl6R1QEQ2HUdvuhmQQJc49V8C2xeoO50fL2gaFJyEaYbphcN9UBhG9ON77GYGkjCgsN1SmouCzrh4xJl-urT2WjRqPAqm_g2e-5SHEdpzznB3TsQ1iXEOtlhCRu-owxVPX8YwwLDWI0qmHMP3d3MzZeuw_vRMohp0O_8G7KKwAiuuslKpSNaQ1RUuRAk8hsYD1IN4ujytiTnqpv9Ati0ndch1_UvwuZmL15KkhzSqkrKnHqNBIgKacwBDBn4m8rad0YxJyR6N4a3T9mSdpmLVT8qiD2PsWBqyZChRFRSfmEzubP7FOQ4b-H6otkuBeXv0KnXVUXzF3Mz6HaYzR5A",
  "tokenTest" : "rLtt6JWvbUHDDhsZnfpAhpYk4dxYDQkbcPTyGaKp2TYqQgG7FGZ5Th_WD53Oq8Ebz6A53njUoo1w3pjU1D4vs_ZMqFiz_j0urb_BH9Oq9VZoKFoJEDAbRZepGcQanImyYrry7Kt6MnMdgfG5jn4HngWoRdKduNNyP4kzcp3mRv7x00ahkm9LAK7ZRieg7k1PDAnBIOG3EyVSJ5kK4WLMvYr7sCwHbHcu4A5WwelxYK0GMJy37bNAarSJDFQsJ2ZvJjvMDmfWwDVFEVe_5tOomfVNt6bOg9mexbGjMrnHBnKnZR1vQbBtQieDlQepzTZMuQrSuKn-t5XZM7V6fCW7oP-uXGX-sMOajeX65JOf6XVpk29DP6ro8WTAflCDANC193yof8-f5_EYY-3hXhJj7RBXmizDpneEQDSaSz5sFk0sV5qPcARJ9zGG73vuGFyenjPPmtDtXtpx35A-BVcOSBYVIWe9kndG3nclfefjKEuZ3m4jL9Gg1h2JBvmXSMYiZtp9MR5I6pvbvylU_PP5xJFSjVTIz7IQSjcVGO41npnwIxRXNRxFOdIUHn0tjQ-7LwvEcTXyPsHXcMD8WtgBh-wxR8aKX7WPSsT1O8d8reb2aR7K3rkV3K82K_0OgawImEpwSvp9MNKynEAJQS6ZHe_J_l77652xwPNxMRTMASk1ZsJL",
  "paymentMethodId": "myfatoorah_gateway",
  "enabled": true,
  "successUrl":"https://toys.accelray.com/payment_response/en/payment_success.html",
  "errorUrl":"https://toys.accelray.com/payment_response/en/payment_failure.html"
};

/// config for after shipping
const afterShip = {
  "api": "e2e9bae8-ee39-46a9-a084-781d0139274f",
  "tracking_url": "https://fluxstore.aftership.com"
};

/// Limit the country list from Billing Address
/// []: default show all country
const List DefaultCountry = [];

//const List DefaultCountry = [
//  {
//    "name": "Vietnam",
//    "iosCode": "VN",
//    "icon": "https://cdn.britannica.com/41/4041-004-A06CBD63/Flag-Vietnam.jpg"
//  },
//  {
//    "name": "India",
//    "iosCode": "IN",
//    "icon":
//        "https://upload.wikimedia.org/wikipedia/en/thumb/4/41/Flag_of_India.svg/1200px-Flag_of_India.svg.png"
//  },
//  {"name": "Austria", "iosCode": "AT", "icon": ""},
//];
