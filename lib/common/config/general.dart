import '../../common/constants.dart';

/// Default app config, it's possible to set as URL
const kAppConfig = 'lib/config/config_en.json';

/// This option is determine hide some components for web
var kLayoutWeb = true;

/// The Google API Key to support Pick up the Address automatically
/// We recommend to generate both ios and android to restrict by bundle app id
/// The download package is remove these keys, please use your own key
const kGoogleAPIKey = {
  "android": "AIzaSyAS7sfLTjQfNrqTq811SJyaq3tvP3ch2yA",
  "ios": "AIzaSyAS7sfLTjQfNrqTq811SJyaq3tvP3ch2yA",
  "web": "AIzaSyAS7sfLTjQfNrqTq811SJyaq3tvP3ch2yA"
};

//const kGoogleAPIKey = {
//  "android": "AIzaSyAM1oJFq-5A9h2ltOMyZEdcfCcXm6KcNKM",
//  "ios": "your-google-api-key",
//  "web": "your-google-api-key"
//};

/// user for upgrader version of app, remove the comment from lib/app.dart to enable this feature
/// https://tppr.me/5PLpD
const kUpgradeURLConfig = {
  "android":
      "https://play.google.com/store/apps/details?id=app.store.teddytoyshouse",
  "ios": "https://apps.apple.com/us/app/mstore-flutter/id1538470449"
};

/// use for rating app on store feature
const kStoreIdentifier = {
  "android": "app.store.teddytoyshouse",
  "ios": "1538470449"
};

const kAdvanceConfig = {
  "DefaultLanguage": "en",
  "DetailedBlogLayout": kBlogLayout.halfSizeImageType,
  "EnablePointReward": false,
  "hideOutOfStock": true,
  "EnableRating": false,
  "hideEmptyProductListRating": false,
  "isCaching": false,

  // set kIsResizeImage to true if you have finish running Re-generate image plugin
  "kIsResizeImage": false,

  "GridCount": 3,

  "DefaultCurrency": {
    "symbol": "\KWD ",
    "decimalDigits": 3,
    "symbolBeforeTheNumber": true,
    "currency": "KWD"
  },
  "Currencies": [
    {
      "symbol": "\KWD ",
      "decimalDigits": 3,
      "symbolBeforeTheNumber": true,
      "currency": "KWD"
    }
  ],

  /// Below config is used for Magento store
  "DefaultStoreViewCode": "en",
  "EnableAttributesConfigurableProduct": ["color", "size"],
  "EnableAttributesLabelConfigurableProduct": ["color", "size"]
};

const kLoginSetting = {
  "IsRequiredLogin": false,
  'showAppleLogin': false,
  'showFacebook': false,
  'showSMSLogin': false,
  'showGoogleLogin': false,
};
