/// config splash screen
const kSplashScreen =
    "assets/images/splashscreen.flr"; //const kSplashScreen = "assets/images/splashscreen.png";

const teddyGifSplashScreen =
    "assets/images/anim_10.gif";

const splashScreenBg =
    "assets/images/teddy_bg.jpg";

const teddySplashScreen =
    "assets/images/Teddy.flr";
//const kProductListLayout = [
//  {"layout": "list", "image": "assets/icons/tabs/icon-list.png"},
//  {"layout": "columns", "image": "assets/icons/tabs/icon-columns.png"},
//  {"layout": "card", "image": "assets/icons/tabs/icon-card.png"},
//  {"layout": "horizontal", "image": "assets/icons/tabs/icon-horizon.png"},
//  {"layout": "listTile", "image": "assets/icons/tabs/icon-lists.png"},
//];

const kProductListLayout = [
  {"layout": "list", "image": "assets/icons/tabs/icon-list.png"},
  {"layout": "listTile", "image": "assets/icons/tabs/icon-lists.png"},
];

const kDefaultImage =
    "https://trello-attachments.s3.amazonaws.com/5d64f19a7cd71013a9a418cf/640x480/1dfc14f78ab0dbb3de0e62ae7ebded0c/placeholder.jpg";

const kLogoImage = 'assets/images/logo.png';

const LogoAppBar = 'assets/images/app_bar_logo.png';

const kImgDrawerHeader = 'assets/images/fogg-delivery-1.png';

const kProfileBackground =
    "https://toys.accelray.com/pub/media/wysiwyg/mobapp/app-settings.png";

const String kLogo = 'assets/images/logo.png';

const String kEmptySearch = 'assets/images/empty_search.png';

const String kOrderCompleted = 'assets/images/fogg-order-completed.png';

const customDrawerIcon = 'assets/images/slider_icon.png';

const customFilterIcon = 'assets/images/filter_icon.png';

/// id_category : image_category
const kGridIconsCategories = {
  23: "assets/icons/categories/i_briefcase.png",
  208: "assets/icons/categories/i_chrome.png",
  24: "assets/icons/categories/i_download.png",
  30: "assets/icons/categories/i_compass.png",
  19: "assets/icons/categories/i_instagram.png",
  21: "assets/icons/categories/i_lib.png",
  25: "assets/icons/categories/i_map.png",
  27: "assets/icons/categories/i_package.png",
  29: "assets/icons/categories/i_shopping.png"
};

class ImageCountry {
  static const String GB = 'assets/images/country/gb.png';
  static const String VN = 'assets/images/country/vn.png';
  static const String JA = 'assets/images/country/ja.png';
  static const String ZH = 'assets/images/country/zh.png';
  static const String ES = 'assets/images/country/es.png';
  static const String AR = 'assets/images/country/ar.png';
  static const String RO = 'assets/images/country/ro.png';
  static const String TR = 'assets/images/country/tr.png';
  static const String IT = 'assets/images/country/it.png';
  static const String ID = 'assets/images/country/id.png';
  static const String DE = 'assets/images/country/de.png';
  static const String BR = 'assets/images/country/br.png';
  static const String FR = 'assets/images/country/fr.png';
}
