export 'config/advertise.dart';
export 'config/general.dart';
export 'config/onboarding.dart';
export 'config/payments.dart';
export 'config/products.dart';
export 'config/smartchat.dart';

/// Server config demo for WooCommerce
/// Get more example for Opencart / Magento / Shopify from the example folder
//const serverConfig = {
//  "type": "magento",
//  "url": "https://accelray.com/magento2",
//  "accessToken": "xsi82znqqjsdbp2yay9ig32ba1r2ipdi",
//  "blog": "http://demo.mstore.io",
//  "forgetPassword": "http://demo.mstore.io/wp-login.php?action=lostpassword"
//};

const serverConfig = {
  "type": "magento",
  "url": "http://167.99.87.51/teddytoys",
  "accessToken": "zo9lp9w88xiv0zk2x8u8qgb6yl17qp58",
  "blog": "http://167.99.87.51/teddytoys",
  "forgetPassword":
      "http://167.99.87.51/teddytoys/customer/account/forgotpassword/"
};
/*
const serverConfig = {
  "type": "magento",
  "url": "https://toys.accelray.com",
  "accessToken": "zo9lp9w88xiv0zk2x8u8qgb6yl17qp58",
  "blog": "https://toys.accelray.com",
  "forgetPassword": "https://toys.accelray.com/customer/account/forgotpassword/"
};*/
//http://167.99.87.51/teddytoys/
//const serverConfig = {
//  "type": "magento",
//  "url": "https://accelray.com/smartstore",
//  "accessToken": "95k1qrl4jez7koynzup2vjyoty1mzjib",
//  "blog": "https://accelray.com/smartstore",
//  "forgetPassword": "https://accelray.com/smartstore/customer/account/forgotpassword/"
//};
