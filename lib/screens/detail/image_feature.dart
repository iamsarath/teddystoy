import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:teddystoy/common/constants.dart';

import '../../common/config.dart';
import '../../common/tools.dart';
import '../../models/product/product_variation.dart';
import '../../models/product/product_model.dart';
import '../../models/product/product.dart';
import '../../widgets/common/image_video_gallery.dart';

class ImageFeature extends StatelessWidget {
  final Product product;

  ImageFeature(this.product);

  @override
  Widget build(BuildContext context) {
    ProductVariation productVariation;
    productVariation = Provider.of<ProductModel>(context).productVariation;
//    final imageFeature = productVariation != null
//        ? productVariation.imageFeature
//        : product.imageFeature;

    final imageFeature = productVariation != null
        ? productVariation.imageFeature
        : product.images[1];

//    printLog("product.images[0]--> ${product.images[0]}");
//    printLog("product.images[1]--> ${product.images[1]}");
//    printLog("imageFeature--> $imageFeature");

    _onShowGallery(context, [index = 0]) {
      Navigator.push(
        context,
        PageRouteBuilder(pageBuilder: (context, __, ___) {
          return ImageVideoGallery(images: product.images, index: index);
        }),
      );
    }

    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return FlexibleSpaceBar(
          background: GestureDetector(
            onTap: () => _onShowGallery(context),
            child: Container(
              child: Card(
                margin: EdgeInsets.all(8.0),
                clipBehavior: Clip.antiAlias,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 5,
                child: Stack(
                  children: <Widget>[
                    kProductDetail['isHero']
                        ? Positioned(
                            top: double.parse(
                                kProductDetail['marginTop'].toString()),
                            child: Hero(
                              tag: 'product-${product.id}',
                              child: Tools.image(
                                url: imageFeature,
                                fit: BoxFit.contain,
                                isResize: true,
                                size: kSize.medium,
                                width: constraints.maxWidth,
                                hidePlaceHolder: true,
                              ),
                            ),
                          )
                        : Positioned(
                            top: double.parse(
                                kProductDetail['marginTop'].toString()),
                            child: Tools.image(
                              url: imageFeature,
                              fit: BoxFit.contain,
                              isResize: true,
                              size: kSize.medium,
                              width: constraints.maxWidth,
                              hidePlaceHolder: true,
                            ),
                          ),
                    Positioned(
                      top: double.parse(kProductDetail['marginTop'].toString()),
                      child: Tools.image(
                        url: imageFeature,
                        fit: BoxFit.contain,
                        width: constraints.maxWidth,
                        size: kSize.large,
                        hidePlaceHolder: true,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
