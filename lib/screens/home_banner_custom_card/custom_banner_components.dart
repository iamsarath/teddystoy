import 'dart:async';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:teddystoy/common/constants/loading.dart';

import 'banner_cell.dart';

class BannerComponents {

  /// For Loading Widget
  static Widget loadingWidget(context) => Center(
    child: Container(
      width: 400,
      height: 400,
      child: SpinKitFadingGrid (
        color: Theme.of(context).primaryColor,
        size: 30.0,
      ),
    ),
  );

  /// For Loading Widget
  static Widget loadingWidgetCard(context) => Center(

      child: Card(
        margin: const EdgeInsets.all(3.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6.0),
        ),
        elevation: 2,
        child: Container(
        width: 350,
        height: 200,
        alignment: Alignment(0.0, 0.0),
//          child: Image.asset(kLogoImage, fit: BoxFit.cover),
          child: kLoadingWidget(context),

      ),

    ),

  );

}

Widget GetCarouselStyle(String style, bool autoPlay, double aspectRatio, bool enlargeCenterPage,
    int initialPage) {

  switch(style) {

    case "Normal": {
      return Container(
          child: Column(children: <Widget>[
            CarouselSlider(
                options: CarouselOptions(
                autoPlay: autoPlay,
                aspectRatio: aspectRatio,
                enlargeCenterPage: enlargeCenterPage,
                initialPage: initialPage,
              ),
              items: BannerCell.getNormalBanner,
            ),
          ],
          ));
    }
    break;

    case "CardStyle": {
      return Container(
          child: Column(children: <Widget>[
            CarouselSlider(
                options: CarouselOptions(
                autoPlay: autoPlay,
                aspectRatio: aspectRatio,
                enlargeCenterPage: enlargeCenterPage,
                initialPage: initialPage,
              ),
              items: BannerCell.getCardBanner,
            ),
          ],
          ));
    }
    break;
  }
}