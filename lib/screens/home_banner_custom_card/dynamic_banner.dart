import 'dart:async';
import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../common/constants/general.dart';
import '../../models/app.dart';
import '../../models/banner/banner_model.dart';
import '../../models/product/product.dart';
import '../../screens/home_banner_custom_card/banner_cell.dart';
import '../../screens/home_banner_custom_card/custom_banner_components.dart';
import '../../services/index.dart';

/// The Banner Group type to display the image as multi columns
class DynamicBannerSliderItems extends StatefulWidget {
  final config;

  DynamicBannerSliderItems({this.config, Key key}) : super(key: key);

  @override
  _DynamicStateBannerSlider createState() => _DynamicStateBannerSlider();
}

class _DynamicStateBannerSlider extends State<DynamicBannerSliderItems> {
  int position = 0;
  final Services _service = Services();
  List<BannerModel> bannerMaster;

//  static List<String> bannerImgArrayList = [];
  String message;
  PageController _controller;
  bool autoPlay;
  Timer timer;
  int intervalTime;

  void initState() {

    printLog("widget width---> ${widget.config['width']}");

    autoPlay = widget.config['autoPlay'] ?? false;
    _controller = PageController();
    intervalTime = widget.config['intervalTime'] ?? 3;
//    autoPlayBanner();
//    getBannerList(lang: Provider.of<AppModel>(context, listen: false).locale);
    super.initState();
  }

  Future<List<BannerModel>>  getBannerList({lang}) async {
    try {

      bannerMaster = await _service.getBannerList(lang: lang);
      print("Banner list from banner---> ${bannerMaster}");

      return bannerMaster;
    } catch (err, trace) {
      message = "There is an issue with the app during request the data, "
          "please contact admin for fixing the issues " +
          err.toString();
      print(trace.toString());
    }
  }

  @override
  void dispose() {
    if (timer != null) {
      timer.cancel();
    }

    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder<List<BannerModel>>(
        future: getBannerList(lang: Provider.of<AppModel>(context, listen: false).locale),
        builder: (context, snapshot) {
          printLog("snapshot.hasData = ${snapshot.data}");
          printLog("snapshot.connectionState = ${snapshot.connectionState}");

          List<String> bannerImgArrayList = [];

          if(snapshot.connectionState == ConnectionState.done) {

            if(bannerImgArrayList != null)
              bannerImgArrayList.clear();

            for (var i = 0; i < bannerMaster.length; i++) {

              bannerImgArrayList.add(bannerMaster[i].image);
            }
            return Container(

              child: renderCarouselSlider(context, bannerImgArrayList),
            );
          }
          else{
            return Container(
                child: BannerComponents.loadingWidgetCard(context),
            );
          }

        },
      ),
    );
  }

  renderCarouselSlider(context, List<String> bannerImgArrayList){

    BannerCell(context, bannerMaster, bannerImgArrayList, onBannerClicked);
    return GetCarouselStyle(widget.config['style'], widget.config['autoPlay'], widget.config['aspectRatio'], widget.config['enlargeCenterPage'],
        widget.config['initialPage']);
  }

  onBannerClicked(BuildContext context, int position) {
    // On Banner Click
    Product.showList(
        context: context,
        cateId: "${bannerMaster[position].catalog_category_id}",
        cateName: bannerMaster[position].name);
  }
}


