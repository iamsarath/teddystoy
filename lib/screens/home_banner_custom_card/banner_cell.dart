import 'package:flutter/material.dart';
import 'package:teddystoy/models/banner/banner_model.dart';
//import 'cell_model.dart';
import '../../models/category/category.dart';

class BannerCell {

  static List<String> bannerImgArrayList_ = [];
  static List<BannerModel> bannerMaster_;
  static double bannerHeight_;

  static BuildContext context_;

  static Function onImgClicked_;

  BannerCell (context, List<BannerModel> bannerMaster, List<String> bannerImgArrayList, Function onImgClicked){

    context_              = context;
    bannerImgArrayList_   = bannerImgArrayList;
    onImgClicked_         = onImgClicked;
    bannerMaster_         = bannerMaster;
  }

  static List<Widget> getNormalBanner = bannerImgArrayList_.map((item) => Container(
      child: Center(
          child: Image.network(item, fit: BoxFit.cover, height: getImageDynamicHeight(context_), width: 1000)
      ),
    )).toList();

  static List<Widget> getCardBanner = bannerImgArrayList_.map((item) => Container(

    child: Container(
        margin: EdgeInsets.all(0),
        child: Card(
            margin: const EdgeInsets.all(3.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(6.0),
            ),
            elevation: 0.3,
            child: GestureDetector(
              onTap: () => onImgClicked_(context_, bannerImgArrayList_.indexOf(item)),
              child: Container(
                  padding: EdgeInsets.all(6.0),
                  child: Stack(
                    children: <Widget>[
                      Image.network(item, fit: BoxFit.cover, height: getImageDynamicHeight(context_), width: 1000),
                      Positioned(
                        bottom: 0.0,
                        left: 0.0,
                        right: 0.0,
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Color.fromARGB(200, 0, 0, 0),
                                Color.fromARGB(0, 0, 0, 0)
                              ],
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                            ),
                          ),
                          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
                          child: Text(
                            '${bannerMaster_[bannerImgArrayList_.indexOf(item)].name}',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),

                      ),
                    ],
                  )
              ),
            )
        )
    ),
  )).toList();

  static double getImageDynamicHeight(context){

    int numImgBlocks = 1;
    var _cellWidth = MediaQuery.of(context).size.width/numImgBlocks;
    var _cellHeight = _cellWidth - 50;
    return _cellHeight;
  }
}