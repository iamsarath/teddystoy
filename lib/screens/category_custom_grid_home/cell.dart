import 'package:flutter/material.dart';
//import 'cell_model.dart';
import '../../models/category/category.dart';

class Cell extends StatelessWidget {
  const Cell(this.category);
  @required
  final Category category;

  @override
  Widget build(BuildContext context) {
    return Center(
     child: Card(
       margin: const EdgeInsets.all(6.0),
       shape: RoundedRectangleBorder(
         borderRadius: BorderRadius.circular(5.0),
       ),
       elevation: 0.5,
      child: Container(
        width: 200,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  padding: EdgeInsets.all(6.0),
                  child: Image.network(category.image,
                      fit: BoxFit.fill
                  )
              ),
            ],
          ),
      ),
     )
    );
  }
}