import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:teddystoy/common/constants/general.dart';
import 'package:teddystoy/models/app.dart';
import 'package:teddystoy/models/category/category.dart';
import 'package:teddystoy/services/index.dart';
import 'category_components.dart';
import '../../models/product/product.dart';

class CategoryGrid extends StatefulWidget {
  CategoryGrid({Key key}) : super(key: key);

  @override
  _CategoryGridState createState() => new _CategoryGridState();
}

class _CategoryGridState extends State<CategoryGrid> {
  bool isHomeDataLoading;

  final Services _service = Services();
  List<Category> categories;

  bool isLoading = false;
  String message;

  @override
  void initState() {
    super.initState();
    isHomeDataLoading = false;

//    _initPlatformState();
  }

//  // Platform messages are asynchronous, so we initialize in an async method.
//  void _initPlatformState() async {
//    String platformVersion;
//    // Platform messages may fail, so we use a try/catch PlatformException.
//    try {
//      platformVersion = await GetVersion.platformVersion;
//      printLog("platformVersion--> ${platformVersion}");
//    } on PlatformException {
//      platformVersion = 'Failed to get platform version.';
//    }
//
//    String projectVersion;
//    // Platform messages may fail, so we use a try/catch PlatformException.
//    try {
//      projectVersion = await GetVersion.projectVersion;
//      printLog("projectVersion--> ${projectVersion}");
//    } on PlatformException {
//      projectVersion = 'Failed to get project version.';
//    }
//
//    String projectCode;
//    // Platform messages may fail, so we use a try/catch PlatformException.
//    try {
//      projectCode = await GetVersion.projectCode;
//      printLog("projectCode--> ${projectCode}");
//    } on PlatformException {
//      projectCode = 'Failed to get build number.';
//    }
//
//    String projectAppID;
//    // Platform messages may fail, so we use a try/catch PlatformException.
//    try {
//      projectAppID = await GetVersion.appID;
//      printLog("projectAppID--> ${projectAppID}");
//    } on PlatformException {
//      projectAppID = 'Failed to get app ID.';
//    }
//
//    String projectName;
//    // Platform messages may fail, so we use a try/catch PlatformException.
//    try {
//      projectName = await GetVersion.appName;
//      printLog("projectName--> ${projectName}");
//    } on PlatformException {
//      projectName = 'Failed to get app name.';
//    }
//
//    // If the widget was removed from the tree while the asynchronous platform
//    // message was in flight, we want to discard the reply rather than calling
//    // setState to update our non-existent appearance.
//
//  }

  Future<List<Category>> getCategories({lang}) async {
    try {

      categories = await _service.getCategories(lang: lang);
      return categories;
    } catch (err, trace) {
      message = "There is an issue with the app during request the data, "
          "please contact admin for fixing the issues " +
          err.toString();
      print(trace.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: FutureBuilder<List<Category>>(
          future: getCategories(lang: Provider.of<AppModel>(context, listen: false).locale),
          builder: (context, snapshot) {
            printLog("snapshot.hasData = ${snapshot.data}");
            return snapshot.connectionState == ConnectionState.done
                ? snapshot.hasData
                ? ComComp.homeGrid(snapshot, gridClicked)
                : ComComp.retryButton(fetch)
                : ComComp.gridLoadingBlock(context);
          },
        ),
      );
  }

  setLoading(bool loading) {
    setState(() {
      isHomeDataLoading = loading;
    });
  }

  fetch() {
    setLoading(true);
  }
}

gridClicked(BuildContext context, Category category) {
  // Grid Click

  printLog("old cat id---> ${category.id}");
  printLog("old cat name---> ${category.name}");

  Product.showList(
      context: context,
      cateId: category.id,
      cateName: category.name);
}
