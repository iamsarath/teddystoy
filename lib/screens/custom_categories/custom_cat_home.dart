import 'dart:async';
import 'dart:ui';
import 'dart:convert' as convert;

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/category/custom_category_model.dart';
import '../../common/constants/general.dart';
import '../../models/app.dart';
import '../../models/product/product.dart';
import '../../services/index.dart';
import 'custom_cat_components.dart';

/// The Banner Group type to display the image as multi columns
class CustomCategoryItems extends StatefulWidget {
  final config;
  CustomCategoryItems({this.config, Key key}) : super(key: key);

  @override
  _CustomCategoryItems createState() => _CustomCategoryItems();
}

class _CustomCategoryItems extends State<CustomCategoryItems> {
  int position = 0;
  final Services _service = Services();
  List<CustomCategoryModel> categoryMaster;
  bool isHomeDataLoading;

  String message;
//  int numImgBlocks;

  void initState() {

    printLog("imageBlocks---> ${widget.config['imageBlocks']}");

//    numImgBlocks = widget.config['imageBlocks'];
    isHomeDataLoading = false;
//    loadEmptyCategoryList();
    super.initState();
  }

  Future<List<CustomCategoryModel>>  getCustomCategoriesList({lang}) async {
    try {

      categoryMaster = await _service.getCustomCategoriesList(lang: lang);
      print("Banner list from banner---> ${categoryMaster}");

      return categoryMaster;
    } catch (err, trace) {
      message = "There is an issue with the app during request the data, "
          "please contact admin for fixing the issues " +
          err.toString();
      print(trace.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder<List<CustomCategoryModel>>(
        future: getCustomCategoriesList(lang: Provider.of<AppModel>(context, listen: false).locale),
        builder: (context, snapshot) {
          printLog("snapshot.hasData = ${snapshot.data}");
          return snapshot.connectionState == ConnectionState.done
              ? snapshot.hasData
              ? CustomCategoryComponentsComp.homeGrid(snapshot, context, widget.config, gridClicked)
              : CustomCategoryComponentsComp.retryButton(fetch)
              : CustomCategoryComponentsComp.gridLoadingBlock(context);
        },
      ),
    );
  }

  setLoading(bool loading) {
    setState(() {
      isHomeDataLoading = loading;
    });
  }

  fetch() {
    setLoading(true);
  }

  gridClicked(BuildContext context, CustomCategoryModel category) {
    // Grid Click

    printLog("cat catalog_category_id---> ${category.catalog_category_id}");
    printLog("cat name---> ${category.name}");

    Product.showList(
        context: context,
        cateId: "${category.catalog_category_id}",
        cateName: category.name);
  }
}


