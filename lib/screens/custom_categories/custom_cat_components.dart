import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import '../../models/category/custom_category_model.dart';
import '../../screens/custom_categories/custom_cat_cells.dart';
import 'package:shimmer/shimmer.dart';
import '../../models/category/category.dart';
import 'custom_cat_constants.dart';


class CustomCategoryComponentsComp {

  Padding text(String text, FontWeight fontWeight, double fontSize,
      List padding, Color color, TextOverflow overflow) {
    return Padding(
      padding: EdgeInsets.only(
          left: padding[0],
          right: padding[1],
          top: padding[2],
          bottom: padding[3]),
      child: Text(
        text,
        textAlign: TextAlign.left,
        overflow: overflow,
        style: TextStyle(
          fontWeight: fontWeight,
          fontSize: fontSize,
          color: color,
        ),
      ),
    );
  }

  static GridView gridLoadingBlock(context){
    return GridView.count(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      crossAxisCount: 2 ,
      children: List.generate(6,(index){
        return Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  padding: EdgeInsets.all(2.0),
                  child: Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      enabled: true,
                      child: Card(
                        margin: const EdgeInsets.all(6.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        elevation: 2.5,
                        child: Container(
                          width: 200,
                          height: getContainerDynamicHeight(context),

                        ),
                      )
                  )
              ),
            ],
          ),
//            )
        );

      }),
    );
  }

  static double getContainerDynamicHeight(context){

    int numImgBlocks = 2;
    var _cellWidth = MediaQuery.of(context).size.width/numImgBlocks;
    var _cellHeight = _cellWidth-20;
    return _cellHeight;
  }

  /// For Loading Widget
  static Widget loadingWidget(context) => Center(
    child: Container(
      width: 400,
      height: 100,
      child: SpinKitFadingCube(
        color: Theme.of(context).primaryColor,
        size: 20.0,
      ),
    ),
  );

  /* SpinKit Animations
SpinKitRotatingCircle(color: Colors.white)
SpinKitRotatingPlain(color: Colors.white)
SpinKitChasingDots(color: Colors.white)
SpinKitPumpingHeart(color: Colors.white)
SpinKitPulse(color: Colors.white)
SpinKitDoubleBounce(color: Colors.white)
SpinKitWave(color: Colors.white, type: SpinKitWaveType.start)
SpinKitWave(color: Colors.white, type: SpinKitWaveType.center)
SpinKitWave(color: Colors.white, type: SpinKitWaveType.end)
SpinKitThreeBounce(color: Colors.white)
SpinKitWanderingCubes(color: Colors.white)
SpinKitWanderingCubes(color: Colors.white, shape: BoxShape.circle)
SpinKitCircle(color: Colors.white)
SpinKitFadingFour(color: Colors.white)
SpinKitFadingFour(color: Colors.white, shape: BoxShape.rectangle)
SpinKitFadingCube(color: Colors.white)
SpinKitCubeGrid(size: 51.0, color: Colors.white)
SpinKitFoldingCube(color: Colors.white)
SpinKitRing(color: Colors.white)
SpinKitDualRing(color: Colors.white)
SpinKitRipple(color: Colors.white)
SpinKitFadingGrid(color: Colors.white)
SpinKitFadingGrid(color: Colors.white, shape: BoxShape.rectangle)
SpinKitHourGlass(color: Colors.white)
SpinKitSpinningCircle(color: Colors.white)
SpinKitSpinningCircle(color: Colors.white, shape: BoxShape.rectangle)
SpinKitFadingCircle(color: Colors.white)
SpinKitPouringHourglass(color: Colors.white)
   */

  static GestureDetector internetErrorText(Function callback) {
    return GestureDetector(
      onTap: callback,
      child: Center(
        child: Text(MESSAGES.INTERNET_ERROR),
      ),
    );
  }

  static Padding homeGrid(AsyncSnapshot<List<CustomCategoryModel>> snapshot, context, var config, Function gridClicked) {
    return Padding(
      padding:
      EdgeInsets.only(left: 5.0, right: 5.0, bottom: 5.0, top: 5.0),
      child: GridView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: snapshot.data.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: config['imageBlocks']),
//      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(childAspectRatio: getAspectRatio(context, config['imageBlocks'], config['minHeight']), crossAxisCount: config['imageBlocks']),
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            child: CustomCategoryCell(snapshot.data[index], config),
            onTap: () => gridClicked(context, snapshot.data[index]),
          );
        },
      ),
    );
  }

  static FlatButton retryButton(Function fetch) {
    return FlatButton(
      child: Text(
        MESSAGES.INTERNET_ERROR_RETRY,
        textAlign: TextAlign.center,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(fontWeight: FontWeight.normal),
      ),
      onPressed: () => fetch(),
    );
  }
}