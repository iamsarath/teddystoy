import 'package:flutter/material.dart';
import '../../models/category/custom_category_model.dart';

class CustomCategoryCell extends StatelessWidget {
  CustomCategoryCell(this.category, this.config);
  @required
  final CustomCategoryModel category;
  var config;

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Card(
          margin: const EdgeInsets.all(3.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(2.0),
          ),
          elevation: 0.5,
          child: Container(
            width: 1000,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                    padding: EdgeInsets.all(6.0),
                    child: Image.network(category.image,
                        fit: BoxFit.fill
                    )
                ),
              ],
            ),
          ),
        )
    );
  }

//  @override
//  Widget build(BuildContext context) {
//
//      return Center(
//
//          child: Column(
//            crossAxisAlignment: CrossAxisAlignment.center,
//            children: <Widget>[
//              Container(
//                  padding: EdgeInsets.all(2.0),
//                  child: Card(
//                      margin: const EdgeInsets.all(2.0),
//                      shape: RoundedRectangleBorder(
//                        borderRadius: BorderRadius.circular(2.0),
//                      ),
//                      elevation: 1,
//                      child: Container(
//                          width: 1000,
//                          height: getImageDynamicHeight(context, config),
//                          padding: EdgeInsets.all(2.0),
//                          child: Image.network(category.image,
//                              fit: BoxFit.fill
//                          )
//                      )
//                  )
//              ),
//              if(config['showTitle'])
//                Container(
//                  padding: EdgeInsets.symmetric(vertical: 5, horizontal: 0),
//                  child: Align(
//                    alignment: Alignment.bottomCenter,
//                    child: Text(
//                      '${category.name}'.toUpperCase(),
//                      style: TextStyle(
//                        color: Colors.black,
//                        fontSize: config['titleFontSize'],
//                        fontWeight: FontWeight.bold,
//                      ),
//                    ),
//                  ),
//                ),
//            ],
//          ),
//      );
//  }

//  @override
//  Widget build(BuildContext context) {
//    return Center(
//        child: Card(
//          margin: const EdgeInsets.all(2.0),
//          shape: RoundedRectangleBorder(
//            borderRadius: BorderRadius.circular(0),
//          ),
//          elevation: 0,
//          child: Container(
//            width: 1000,
//            child: Column(
//              crossAxisAlignment: CrossAxisAlignment.center,
//              children: <Widget>[
//                Container(
//                    padding: EdgeInsets.all(0.0),
//                    child: Image.network(category.image,
//                        fit: BoxFit.fill
//                    )
//                ),
//                SizedBox(height: 10,),
//                if(config['showTitle'])
//                   Positioned(
//                    child: Align(
//                        alignment: FractionalOffset.bottomCenter,
//                        child: Text(
//                          '${category.name}'.toUpperCase(),
//                          style: TextStyle(
//                            color: Colors.black,
//                            fontSize: config['titleFontSize'],
//                            fontWeight: FontWeight.bold,
//                          ),
//                        ),
//                    ),
//                  )////
//              ],
//            ),
//          ),
//        )
//    );
//  }

  static double getImageDynamicHeight(context, config){

    int numImgBlocks = config['imageBlocks'];
    var _cellWidth = MediaQuery.of(context).size.width/numImgBlocks;
    var _cellHeight = _cellWidth - 50;
    print("_cellWidth---> ${_cellWidth}");
    print("_cellHeight---> ${_cellHeight}");
    return _cellHeight;
  }
}