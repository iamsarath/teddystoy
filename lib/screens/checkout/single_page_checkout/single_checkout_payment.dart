import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
//import 'package:my_fatoorah/my_fatoorah.dart';
import 'package:provider/provider.dart';
import 'package:teddystoy/models/app.dart';
import 'package:teddystoy/screens/cart/my_cart.dart';
import 'package:teddystoy/widgets/payment/myfatoorah/my_fatoorah.dart';

import '../../../common/config.dart';
import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../generated/l10n.dart';
import '../../../models/cart/cart_model.dart';
import '../../../models/payment_method.dart';
import '../../../models/user/user_model.dart';
import '../../../services/index.dart';
import '../../../widgets/payment/paypal/index.dart';
import '../../../widgets/payment/tap/index.dart';
import '../../../widgets/payment/myfatoorah/enums/currency_iso.dart';
import '../../../widgets/payment/myfatoorah/enums/other.dart';
import '../../../widgets/payment/myfatoorah/enums/language.dart';
import '../../../widgets/payment/myfatoorah/request/my_fatoorah_request.dart';
import 'order_success.dart';

class SingleCheckoutPaymentMethods extends StatefulWidget {
  final Function onBack;
  final Function onFinish;
  final Function(bool) onLoading;
  final Widget checkoutWidget;
  final paymentScreenContext;

  SingleCheckoutPaymentMethods(
      {this.onBack,
      this.onFinish,
      this.onLoading,
      this.checkoutWidget,
      this.paymentScreenContext});

  @override
  _SingleCheckoutPaymentMethodsState createState() =>
      _SingleCheckoutPaymentMethodsState();
}

class _SingleCheckoutPaymentMethodsState
    extends State<SingleCheckoutPaymentMethods> {
  String selectedId;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, () {
      final cartModel = Provider.of<CartModel>(context, listen: false);
      final userModel = Provider.of<UserModel>(context, listen: false);
      Provider.of<PaymentMethodModel>(context, listen: false).getPaymentMethods(
          address: Provider.of<CartModel>(context, listen: false).address,
          shippingMethod: cartModel.shippingMethod,
          token: userModel.user != null ? userModel.user.cookie : null);
      printLog(
          "getPaymentMethods1 shippingMethod ----> ${cartModel.shippingMethod}");
    });
  }

  @override
  void didChangeDependencies() {
    /* final cartModel = Provider.of<CartModel>(context, listen: true);
    final userModel = Provider.of<UserModel>(context, listen: false);
    Provider.of<PaymentMethodModel>(context, listen: true).getPaymentMethods(
        address: Provider.of<CartModel>(context, listen: true).address,
        shippingMethod: cartModel.shippingMethod,
        token: userModel.user != null ? userModel.user.cookie : null);*/
    //debugPrint('Child widget: didChangeDependencies(), counter = $_counter');
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final cartModel = Provider.of<CartModel>(context, listen: true);
    final paymentMethodModel = Provider.of<PaymentMethodModel>(context);
    final userModel = Provider.of<UserModel>(context, listen: false);

    return ListenableProvider.value(
        value: paymentMethodModel,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 20),
            Divider(height: 5),
            SizedBox(height: 20),
            Text(S.of(context).paymentMethods,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
            SizedBox(height: 5),
            Text(
              S.of(context).chooseYourPaymentMethod,
              style: TextStyle(
                fontSize: 12,
                color: Theme.of(context).accentColor.withOpacity(0.6),
              ),
            ),
            SizedBox(height: 10),
            Consumer<PaymentMethodModel>(builder: (context, model, child) {
              if (model.isLoading) {
                return Container(height: 100, child: kLoadingWidget(context));
              }

              if (model.message != null) {
                return Container(
                  height: 100,
                  child: Center(
                      child: Text(model.message,
                          style: TextStyle(color: kErrorRed))),
                );
              }

              if (selectedId == null && model.paymentMethods.isNotEmpty) {
                selectedId =
                    model.paymentMethods.firstWhere((item) => item.enabled).id;
              }

              return Card(
                  margin: const EdgeInsets.all(6.0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  elevation: 2.5,
                  child: Column(
                    children: <Widget>[
                      for (int i = 0; i < model.paymentMethods.length; i++)
                        model.paymentMethods[i].enabled
                            ? Column(
                                children: <Widget>[
                                  InkWell(
                                    onTap: () {
                                      setState(() {
                                        selectedId = model.paymentMethods[i].id;
                                      });
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: model.paymentMethods[i].id ==
                                                  selectedId
                                              ? Theme.of(context)
                                                  .primaryColorLight
                                              : Colors.transparent),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 15, horizontal: 10),
                                        child: Row(
                                          children: <Widget>[
                                            Radio(
                                                value:
                                                    model.paymentMethods[i].id,
                                                groupValue: selectedId,
                                                onChanged: (i) {
                                                  setState(() {
                                                    selectedId = i;
                                                  });
                                                }),
                                            SizedBox(width: 10),
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  if (Payments[model
                                                          .paymentMethods[i]
                                                          .id] !=
                                                      null)
                                                    Image.asset(
                                                      Payments[model
                                                          .paymentMethods[i]
                                                          .id],
                                                      width: 120,
                                                      height: 30,
                                                    ),
                                                  if (Payments[model
                                                          .paymentMethods[i]
                                                          .id] ==
                                                      null)
                                                    Text(
                                                      model.paymentMethods[i]
                                                          .title,
                                                      style: TextStyle(
                                                        fontSize: 14,
                                                        color: Theme.of(context)
                                                            .accentColor
                                                            .withOpacity(0.8),
                                                      ),
                                                    ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            : Container()
                    ],
                  ));
            }),
            SizedBox(height: 20),
            Divider(height: 5),
            SizedBox(height: 20),
            Text("Price Details",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
            SizedBox(height: 5),
            Card(
              margin: const EdgeInsets.all(6.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              elevation: 2.5,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 20),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          S.of(context).subtotal,
                          style: TextStyle(
                            fontSize: 14,
                            color:
                                Theme.of(context).accentColor.withOpacity(0.8),
                          ),
                        ),
                        Text(
                            Tools.getCurrecyFormatted(cartModel.getSubTotal(),
                                currency: cartModel.currency),
                            style: TextStyle(fontSize: 14, color: kGrey400))
                      ],
                    ),
                  ),
                  kPaymentConfig['EnableShipping']
                      ? Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 8, horizontal: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "${cartModel.shippingMethod.title}",
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Theme.of(context)
                                      .accentColor
                                      .withOpacity(0.8),
                                ),
                              ),
                              Text(
                                Tools.getCurrecyFormatted(
                                    cartModel.getShippingCost(),
                                    currency: cartModel.currency),
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Theme.of(context)
                                      .accentColor
                                      .withOpacity(0.8),
                                ),
                              )
                            ],
                          ),
                        )
                      : Container(),
                  if (cartModel.getCoupon() != '')
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8, horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            S.of(context).discount,
                            style: TextStyle(
                              fontSize: 14,
                              color: Theme.of(context)
                                  .accentColor
                                  .withOpacity(0.8),
                            ),
                          ),
                          Text(
                            cartModel.getCoupon(),
                            style:
                                Theme.of(context).textTheme.subtitle1.copyWith(
                                      fontSize: 14,
                                      color: Theme.of(context)
                                          .accentColor
                                          .withOpacity(0.8),
                                    ),
                          )
                        ],
                      ),
                    ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          S.of(context).total,
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                        Text(
                          Tools.getCurrecyFormatted(cartModel.getTotal(),
                              currency: cartModel.currency),
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                            decoration: TextDecoration.underline,
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 0),
                  Text(
                    "Deliver in about 2 hours",
                    style: TextStyle(
                      fontSize: 16,
                      color: ThemeColor,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(height: 20),
                ],
              ),
            ),

            SizedBox(height: 15),

            Row(children: [
              Expanded(
                child: ButtonTheme(
                  height: 45,
                  child: RaisedButton(
                    onPressed: () => placeOrder(
                        paymentMethodModel, cartModel, widget.checkoutWidget),
                    textColor: Colors.white,
                    color: Colors.green,
                    child: Text(S.of(context).placeMyOrder.toUpperCase()),
                  ),
                ),
              ),
            ]),
            Center(
              child: FlatButton(
                onPressed: () {
                  widget.onBack();
                },
              ),
            )
          ],
        ));
  }

  void placeOrder(paymentMethodModel, cartModel, checkoutWidget) {
    var totalCartQuantity =
        Provider.of<CartModel>(context, listen: false).totalCartQuantity;
    if (totalCartQuantity == 0) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => MyCart()),
      );
    }
    if (paymentMethodModel.paymentMethods.isNotEmpty) {
      final paymentMethod = paymentMethodModel.paymentMethods
          .firstWhere((item) => item.id == selectedId);

      Provider.of<CartModel>(context, listen: false)
          .setPaymentMethod(paymentMethod);

      printLog("paymentMethod.id---> ${paymentMethod.id}");

      /// Use Native payment
      if (paymentMethod.id.contains(PaypalConfig["paymentMethodId"]) &&
          PaypalConfig["enabled"] == true) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => PaypalPayment(
              onFinish: (number) {
                createOrder(paid: true);
              },
            ),
          ),
        );
      } else if (paymentMethod.id.contains(TapConfig["paymentMethodId"]) &&
          TapConfig["enabled"] == true) {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => TapPayment(onFinish: (number) {
                    createOrder(paid: true);
                  })),
        );
      } else if (paymentMethod.id
              .contains(MyFatoorahConfig["paymentMethodId"]) &&
          MyFatoorahConfig["enabled"] == true) {
        ApiLanguage getApiLanguage() {
          if (Provider.of<AppModel>(context, listen: false).locale == "ar") {
            return ApiLanguage.Arabic;
          } else {
            return ApiLanguage.English;
          }
        }

        MyFatoorah.startPayment(
          context: context,
          request: MyfatoorahRequest(
            currencyIso: Country.Kuwait,
            successUrl: MyFatoorahConfig["successUrl"],
            errorUrl: MyFatoorahConfig["errorUrl"],
            invoiceAmount: cartModel.getTotal(),
            language: getApiLanguage(),
            token: MyFatoorahConfig["isLive"]
                ? MyFatoorahConfig["tokenLive"]
                : null,
            afterPaymentBehaviour: AfterPaymentBehaviour.None,
            //See the describe for this property for more details
          ),
          selectedId: selectedId,
          widgetOrderPage: widget,
          paymentScreenContext: widget.paymentScreenContext,
        );
      } else {
        handlePlaceOrder(context, cartModel, paymentMethod);

//        Services().widget.placeOrder(
//          context,
//          cartModel: cartModel,
//          onLoading: widget.onLoading,
//          paymentMethod: paymentMethod,
//          success: (order) {
//
////            Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
//            Route route = MaterialPageRoute(
//                builder: (context) => OrderSuccess(order: order));
//            Navigator.pushReplacement(context, route);
//
//            widget.onFinish(order);
//            widget.onLoading(false);
//
//          },
//          error: (message) {
//            printLog("line 385 single_checkout_payment.dart---> "+message);
//            widget.onLoading(false);
//            final snackBar = SnackBar(
//              content: Text(message),
//            );
//            Scaffold.of(context).showSnackBar(snackBar);
//          },
//        );
      }
    }
  }

  Future<void> handlePlaceOrder(
      BuildContext context, cartModel, paymentMethod) async {
    try {
      Dialogs.showLoadingDialog(context, _keyLoader, "Please wait...");

      Services().widget.placeOrder(
        context,
        cartModel: cartModel,
        onLoading: widget.onLoading,
        paymentMethod: paymentMethod,
        success: (order) {
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          Route route = MaterialPageRoute(
              builder: (context) => OrderSuccess(order: order));
          Navigator.pushReplacement(context, route);

          widget.onFinish(order);
          widget.onLoading(false);
        },
        error: (message) {
          printLog("line 385 single_checkout_payment.dart---> " + message);
          widget.onLoading(false);
          final snackBar = SnackBar(
            content: Text(message),
          );
          Scaffold.of(context).showSnackBar(snackBar);
        },
      );
    } catch (error) {
      print(error);
    }
  }

  void placeOrderAfterPayment() {
    final paymentMethodModel = Provider.of<PaymentMethodModel>(context);
    final paymentMethod = paymentMethodModel.paymentMethods
        .firstWhere((item) => item.id == selectedId);
    final cartModel = Provider.of<CartModel>(context);

    Services().widget.placeOrder(
      context,
      cartModel: cartModel,
      onLoading: widget.onLoading,
      paymentMethod: paymentMethod,
      success: (order) {
        widget.onFinish(order);
        widget.onLoading(false);
      },
      error: (message) {
        widget.onLoading(false);
        final snackBar = SnackBar(
          content: Text(message),
        );
        Scaffold.of(context).showSnackBar(snackBar);
      },
    );
  }

  Future<void> createOrder({paid = false, cod = false}) async {
    widget.onLoading(true);
    await Services().widget.createOrder(
      context,
      paid: paid,
      cod: cod,
      success: (order) {
        widget.onFinish(order);
        widget.onLoading(false);
      },
      error: (message) {
        widget.onLoading(false);
        final snackBar = SnackBar(
          content: Text(message),
        );
        Scaffold.of(context).showSnackBar(snackBar);
      },
    );
  }
}
