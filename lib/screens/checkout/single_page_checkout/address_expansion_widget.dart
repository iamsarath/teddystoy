import 'package:flutter/material.dart';
import 'package:configurable_expansion_tile/configurable_expansion_tile.dart';

class ExpansionAddress extends StatelessWidget {
  final String rawTitle;
  final String expansionTitle;
  final bool expand;
  final List<Widget> children;

  ExpansionAddress({@required this.rawTitle, @required this.expansionTitle, @required this.children, this.expand = false});

  @override
  Widget build(BuildContext context) {
    return ConfigurableExpansionTile(
      initiallyExpanded: true,
//      borderColorStart: Colors.blue,
//      borderColorEnd: Colors.orange,
      animatedWidgetFollowingHeader: const Icon(
        Icons.expand_more,
        color: Colors.black,
      ),
//      headerBackgroundColorStart: Colors.grey,
//      expandedBackgroundColor: Colors.amber,
//      headerBackgroundColorEnd: Colors.teal,
      headerExpanded: Flexible(
        child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 20.0),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
              Text(expansionTitle, style: TextStyle( fontSize: 18, color: Colors.black)),
//              Icon(
//                Icons.keyboard_arrow_up,
//                color: Colors.black,
//                size: 20,
//              )
            ])),
      ),
      header: Flexible(
        child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 14.0),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
              Text(rawTitle, style: TextStyle(
                fontSize: 16,
                color: Colors.black,
              )),
//              Icon(
//                Icons.keyboard_arrow_right,
//                color: Theme.of(context).accentColor,
//                size: 20,
//              )
            ])),
      ),
      children: children,
    );
  }
}
