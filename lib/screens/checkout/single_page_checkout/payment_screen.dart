import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:teddystoy/common/config.dart';
import 'package:teddystoy/common/constants/general.dart';
import 'package:teddystoy/common/constants/loading.dart';
import 'package:teddystoy/generated/l10n.dart';
import 'package:teddystoy/models/cart/cart_model.dart';
import 'package:teddystoy/models/order/order.dart';
import 'package:teddystoy/screens/checkout/single_page_checkout/single_checkout_review.dart';
import 'package:teddystoy/screens/checkout/single_page_checkout/single_checkout_shipping_methods.dart';
import '../payment.dart';
import '../review.dart';
import '../shipping_address.dart';
import '../shipping_method.dart';
import '../success.dart';
import 'delivery_address.dart';

class PaymentScreen extends StatefulWidget {
//  final PageController controller;
//  final bool isModal;

//  SinglePageCheckout({this.controller, this.isModal});
  PaymentScreen();

  @override
  _PaymentScreenState createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  int tabIndex = 0;
  Order newOrder;
  bool isPayment = false;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    print("_PaymentScreenState ");
  }

  void setLoading(bool loading) {
    setState(() {
      isLoading = loading;
    });
  }

  @override
  Widget build(BuildContext context) {
    final cartModel = Provider.of<CartModel>(context);

    return Stack(
      children: <Widget>[
        Scaffold(
          backgroundColor: Theme.of(context).backgroundColor,
          appBar: AppBar(
            backgroundColor: Theme.of(context).backgroundColor,
            title: Text(
              "Proceed to pay",
              style: TextStyle(
                color: Theme.of(context).accentColor,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
          body: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          child: ListView(
                            padding: const EdgeInsets.only(top: 0, bottom: 10),
                            children: <Widget>[
                              SingleCheckoutShippingMethods(
                                  onBack: () {
                                    setState(() {
//                                  tabIndex -= 1;
                                    });
                                  },
                                  onNext: () {
                                    setState(() {
//                                  tabIndex = 2;
                                    });
                                  },
                                  onLoading: setLoading,
                                  checkoutWidget: widget,
                                  paymentScreenContext: context),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        isLoading
            ? Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                color: Colors.white.withOpacity(0.36),
                child: kLoadingWidget(context),
              )
            : Container()
      ],
    );
  }
}
