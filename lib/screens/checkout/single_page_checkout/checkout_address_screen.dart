import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:teddystoy/common/config.dart';
import 'package:teddystoy/common/constants/general.dart';
import 'package:teddystoy/common/constants/loading.dart';
import 'package:teddystoy/generated/l10n.dart';
import 'package:teddystoy/models/address.dart';
import 'package:teddystoy/models/cart/cart_model.dart';
import 'package:teddystoy/models/order/order.dart';
import 'package:teddystoy/models/shipping_method.dart';
import 'package:teddystoy/models/user/user.dart';
import 'package:teddystoy/models/user/user_model.dart';
import 'package:teddystoy/screens/checkout/single_page_checkout/payment_screen.dart';
import 'package:teddystoy/services/index.dart';
import '../payment.dart';
import '../review.dart';
import '../shipping_address.dart';
import '../shipping_method.dart';
import '../success.dart';
import 'delivery_address.dart';


class CheckoutAddressScreen extends StatefulWidget {
//  final PageController controller;
//  final bool isModal;

//  SinglePageCheckout({this.controller, this.isModal});
  CheckoutAddressScreen();

  @override
  _CheckoutAddressScreenState createState() => _CheckoutAddressScreenState();
}

class _CheckoutAddressScreenState extends State<CheckoutAddressScreen> with AfterLayoutMixin {
  int tabIndex = 0;
  Order newOrder;
  bool isPayment = false;
  bool isLoading = false;
  Address address;

  @override
  void initState() {
    super.initState();
  }

  void setLoading(bool loading) {
    setState(() {
      isLoading = loading;
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    if (!kPaymentConfig['EnableAddress']) {
      setState(() {
        tabIndex = 1;
      });
      if (!kPaymentConfig['EnableShipping']) {
        setState(() {
          tabIndex = 2;
        });
        if (!kPaymentConfig['EnableReview']) {
          setState(() {
            tabIndex = 3;
            isPayment = true;
          });
        }
      }
    }
  }

  /// Load Shipping beforehand
  void _loadShipping() async {
    final shippingMethodModel = Provider.of<ShippingMethodModel>(context);
    // await Services().widget.loadShippingMethods(context, address, true);
    if (shippingMethodModel?.shippingMethods?.isEmpty ?? true) {
      Services().widget.loadShippingMethods(context, address, true);
    }

    //_loadPayments();
  }

  @override
  Widget build(BuildContext context) {
    final cartModel = Provider.of<CartModel>(context);

    return Stack(
      children: <Widget>[
        Scaffold(
          backgroundColor: Theme
              .of(context)
              .backgroundColor,
          appBar: AppBar(
            backgroundColor: Theme
                .of(context)
                .backgroundColor,
            title: Text(
              "Shipping Details",
              style: TextStyle(
                color: Theme
                    .of(context)
                    .accentColor,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
          body: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          child: ListView(
                            padding: const EdgeInsets.only(
                                top: 20, bottom: 10),
                            children: <Widget>[
                              DeliveryAddress(onNext: () {

//                                ////////////////////////////////////
//                                /// Load Shipping beforehand
//                                Future.delayed(
//                                  Duration.zero,
//                                      () async {
//                                    final addressValue =
//                                    await Provider.of<CartModel>(context, listen: false).getAddress();
//
//                                    if (addressValue != null) {
//                                      setState(() {
//                                        address = addressValue;
//                                        //printLog("addressvaluecity ------ ${address.city}");
//                                      });
//                                    } else {
//                                      User user = Provider.of<UserModel>(context, listen: false).user;
//                                      setState(() {
//                                        address = Address(country: kPaymentConfig["DefaultCountryISOCode"]);
//                                      });
//                                    }
//                                    //await Services().widget.loadShippingMethods(context, address, false);
//                                    Provider.of<CartModel>(context, listen: false).setAddress(address);
//                                    await _loadShipping();
//                                    /*  Services().widget.loadShippingMethods(context,
//            Provider.of<CartModel>(context, listen: false).address, false);*/
//
//                                    printLog("address from checkout_address_screen.dart line 149 ------ ${address}");
//
//                                  },
//                                );
//////////////////////////////////////////////////////////////////

                                Future.delayed(Duration.zero, () {
                                  Route route = MaterialPageRoute(
                                      builder: (context) => PaymentScreen());
                                  Navigator.pushReplacement(context, route);
                                });
                              })
                            ],
                          ),
                        )

//                        Expanded(
//                          child: ListView(
//                            padding: const EdgeInsets.only(
//                                top: 20, bottom: 10),
//                            children: <Widget>[renderContent()],
//                          ),
//                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        isLoading
            ? Container(
          height: MediaQuery
              .of(context)
              .size
              .height,
          width: MediaQuery
              .of(context)
              .size
              .width,
          color: Colors.white.withOpacity(0.36),
          child: kLoadingWidget(context),
        )
            : Container()
      ],
    );
  }
}
