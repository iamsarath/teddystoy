import 'package:country_pickers/country_pickers.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/config.dart';
import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../generated/l10n.dart';
import '../../../models/cart/cart_model.dart';
import '../../../models/product/product.dart';
import '../../../widgets/common/expansion_info.dart';
import '../../../widgets/product/cart_item.dart';

class SingleCheckoutReview extends StatefulWidget {
  final Function onBack;
  final Function onNext;
  final onLoading;

  SingleCheckoutReview({this.onBack, this.onNext, this.onLoading});

  @override
  _SingleCheckoutReviewState createState() => _SingleCheckoutReviewState();
}

class _SingleCheckoutReviewState extends State<SingleCheckoutReview> {
  TextEditingController note = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Consumer<CartModel>(
      builder: (context, model, child) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 20),
            Divider(height: 5),
            SizedBox(height: 20),
            Text(S.of(context).orderDetail,
                style: TextStyle( fontSize: 18, fontWeight: FontWeight.w500)),
            SizedBox(height: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ...getProducts(model, context),
              ]),

            SizedBox(height: 20),
            Divider(height: 5),
            SizedBox(height: 20),

            Text(
              S.of(context).yourNote,
              style: TextStyle( fontSize: 18, fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 6,
            ),
            Card(
              margin: const EdgeInsets.all(6.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              elevation: 2.5,
              child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: TextField(
                    maxLines: 5,
                    controller: note,
                    style: TextStyle(fontSize: 13),
                    decoration: InputDecoration(
                        hintText: S.of(context).writeYourNote,
                        hintStyle: TextStyle(fontSize: 12),
                        border: InputBorder.none),
                  )),
            ),
          ],
        );
      },
    );
  }

  List<Widget> getProducts(CartModel model, BuildContext context) {
    return model.productsInCart.keys.map(
          (key) {
        String productId = Product.cleanProductID(key);

        Product product = model.getProductById(productId);
        return ShoppingCartRow(
          product: model.getProductById(productId),
          variation: model.getProductVariationById(key),
          quantity: model.productsInCart[key],
          onRemove: () {
            model.removeItemFromCart(key);
          },
          onChangeQuantity: (val) {
            String message = Provider.of<CartModel>(context, listen: false)
                .updateQuantity(product, key, val);
            if (message.isNotEmpty) {
              final snackBar = SnackBar(
                content: Text(message),
                duration: Duration(seconds: 1),
              );
              Future.delayed(Duration(milliseconds: 300),
                      () => Scaffold.of(context).showSnackBar(snackBar));
            }
          },
        );
      },
    ).toList();
  }
}

//class ShippingAddressInfo extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    final cartModel = Provider.of<CartModel>(context);
//    final address = cartModel.address;
//
//    return Container(
//      color: Theme.of(context).cardColor,
//      padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
//      child: Column(
//        children: <Widget>[
//          Padding(
//            padding: const EdgeInsets.symmetric(vertical: 5),
//            child: Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              crossAxisAlignment: CrossAxisAlignment.center,
//              children: <Widget>[
//                Container(
//                  width: 120,
//                  child: Text(
//                    S.of(context).firstName + " :",
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                ),
//                Expanded(
//                  child: Text(
//                    address.firstName,
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                )
//              ],
//            ),
//          ),
//          Padding(
//            padding: const EdgeInsets.symmetric(vertical: 5),
//            child: Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              crossAxisAlignment: CrossAxisAlignment.center,
//              children: <Widget>[
//                Container(
//                  width: 120,
//                  child: Text(
//                    S.of(context).lastName + " :",
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                ),
//                Expanded(
//                  child: Text(
//                    address.lastName,
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                )
//              ],
//            ),
//          ),
//          Padding(
//            padding: const EdgeInsets.symmetric(vertical: 5),
//            child: Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              crossAxisAlignment: CrossAxisAlignment.center,
//              children: <Widget>[
//                Container(
//                  width: 120,
//                  child: Text(
//                    S.of(context).email + " :",
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                ),
//                Expanded(
//                  child: Text(
//                    address.email,
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                )
//              ],
//            ),
//          ),
//          Padding(
//            padding: const EdgeInsets.symmetric(vertical: 5),
//            child: Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              crossAxisAlignment: CrossAxisAlignment.center,
//              children: <Widget>[
//                Container(
//                  width: 120,
//                  child: Text(
//                    S.of(context).streetName + " :",
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                ),
//                Expanded(
//                  child: Text(
//                    address.street,
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                )
//              ],
//            ),
//          ),
//          Padding(
//            padding: const EdgeInsets.symmetric(vertical: 5),
//            child: Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              crossAxisAlignment: CrossAxisAlignment.center,
//              children: <Widget>[
//                Container(
//                  width: 120,
//                  child: Text(
//                    S.of(context).city + " :",
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                ),
//                Expanded(
//                  child: Text(
//                    address.city,
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                )
//              ],
//            ),
//          ),
//          Padding(
//            padding: const EdgeInsets.symmetric(vertical: 5),
//            child: Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              crossAxisAlignment: CrossAxisAlignment.center,
//              children: <Widget>[
//                Container(
//                  width: 120,
//                  child: Text(
//                    S.of(context).stateProvince + " :",
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                ),
//                Expanded(
//                  child: Text(
//                    address.state,
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                )
//              ],
//            ),
//          ),
//          Padding(
//            padding: const EdgeInsets.symmetric(vertical: 5),
//            child: Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              crossAxisAlignment: CrossAxisAlignment.center,
//              children: <Widget>[
//                Container(
//                  width: 120,
//                  child: Text(
//                    S.of(context).country + " :",
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                ),
//                Expanded(
//                  child: Text(
//                    CountryPickerUtils.getCountryByIsoCode(address.country)
//                        .name,
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                )
//              ],
//            ),
//          ),
//          Padding(
//            padding: const EdgeInsets.symmetric(vertical: 5),
//            child: Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              crossAxisAlignment: CrossAxisAlignment.center,
//              children: <Widget>[
//                Container(
//                  width: 120,
//                  child: Text(
//                    S.of(context).phoneNumber + " :",
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                ),
//                Expanded(
//                  child: Text(
//                    address.phoneNumber,
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                )
//              ],
//            ),
//          ),
//          SizedBox(height: 20)
//        ],
//      ),
//    );
//  }
//}
