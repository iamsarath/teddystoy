import 'package:country_pickers/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:quiver/strings.dart';
import 'package:teddystoy/common/config/payments.dart';
import 'package:teddystoy/models/order/order.dart';
import 'package:teddystoy/models/payment_method.dart';
import 'package:teddystoy/screens/checkout/review.dart';
import 'package:teddystoy/screens/checkout/single_page_checkout/payment_screen.dart';
import 'package:teddystoy/screens/checkout/single_page_checkout/single_checkout_payment.dart';
import 'package:teddystoy/screens/checkout/single_page_checkout/single_checkout_review.dart';
import 'package:teddystoy/widgets/common/expansion_info.dart';

import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../generated/l10n.dart';
import '../../../models/cart/cart_model.dart';
import '../../../models/shipping_method.dart';
import '../../../services/index.dart';
import '../../../models/address.dart';
import '../../../models/user/user_model.dart';
import 'checkout_address_screen.dart';

class SingleCheckoutShippingMethods extends StatefulWidget {
  final Function onBack;
  final Function onNext;
  final onLoading;
  final Widget checkoutWidget;
  final paymentScreenContext;

  SingleCheckoutShippingMethods(
      {this.onBack,
      this.onNext,
      this.onLoading,
      this.checkoutWidget,
      this.paymentScreenContext});

  @override
  _SingleCheckoutShippingMethodsState createState() =>
      _SingleCheckoutShippingMethodsState();
}

class _SingleCheckoutShippingMethodsState
    extends State<SingleCheckoutShippingMethods> {
  int selectedIndex = 0;
  bool isLoading = false;
  Order newOrder;
  Address address;
  String firstName;
  String lastName;
  String city;
  String addressdisplaytext;

  @override
  void initState() {
    super.initState();
    printLog(
        "spm: address--> ${Provider.of<CartModel>(context, listen: false).address}");
    //printLog("sp: context--> ${context}");

//    Future.delayed(Duration.zero, () async {
//
//      final addressValue = await Provider.of<CartModel>(context, listen: false).getAddress();
//      printLog("addressValue--> $addressValue");
//    });

    /*Future.delayed(Duration.zero, () async {
//      final addressValue = await Provider.of<CartModel>(context, listen: false).getAddress();
//      printLog("addressValue--> $addressValue");
      printLog(
          "sp: address--> ${Provider.of<CartModel>(context, listen: false).address}");
      Services().widget.loadShippingMethods(context,
          Provider.of<CartModel>(context, listen: false).address, false);
//      printLog("shippingMethod#==> ${Provider.of<ShippingMethodModel>(context).shippingMethods[selectedIndex]}");
    });*/
    Future.delayed(
      Duration.zero,
      () async {
        final addressValue =
            await Provider.of<CartModel>(context, listen: false).getAddress();

        if (addressValue != null) {
          setState(() {
            address = addressValue;
            //printLog("addressvaluecity ------ ${address.city}");
          });
        } else {
          User user = Provider.of<UserModel>(context, listen: false).user;
          setState(() {
            address = Address(country: kPaymentConfig["DefaultCountryISOCode"]);
          });
        }
        //await Services().widget.loadShippingMethods(context, address, false);
        Provider.of<CartModel>(context, listen: false).setAddress(address);
        await _loadShipping();
        /*  Services().widget.loadShippingMethods(context,
            Provider.of<CartModel>(context, listen: false).address, false);*/
        setState(() {});
        printLog("address1 ------ ${address}");

        firstName = address?.firstName ?? '';
        lastName = address?.lastName ?? '';
        city = address?.state ?? '';
      },
    );

    /*Future.delayed(Duration.zero, () async {
      Services().widget.loadShippingMethods(context,
          Provider.of<CartModel>(context, listen: false).address, false);
    });*/
  }

  void setLoading(bool loading) {
    setState(() {
      isLoading = loading;
    });
  }

  /// Load Shipping beforehand
  void _loadShipping() async {
    printLog("===> _loadShipping()");
    final shippingMethodModel = Provider.of<ShippingMethodModel>(context, listen: false);
    // await Services().widget.loadShippingMethods(context, address, true);

    if (shippingMethodModel?.shippingMethods?.isEmpty ?? true) {
      Services().widget.loadShippingMethods(context, address, true);
    }
    //_loadPayments();
  }

  void _loadPayments() async {
    final cartModel = Provider.of<CartModel>(context, listen: true);
    final userModel = Provider.of<UserModel>(context, listen: false);
    Provider.of<PaymentMethodModel>(context, listen: true).getPaymentMethods(
        address: Provider.of<CartModel>(context, listen: true).address,
        shippingMethod: cartModel.shippingMethod,
        token: userModel.user != null ? userModel.user.cookie : null);
  }

  @override
  Widget build(BuildContext context) {
    final shippingMethodModel = Provider.of<ShippingMethodModel>(context);
    final currency = Provider.of<CartModel>(context).currency;
    final totalMoney = Provider.of<CartModel>(context).getTotal();
    final cartModel = Provider.of<CartModel>(context);

//    printLog(
//        "shippingMethodModel==> ${shippingMethodModel.shippingMethods[selectedIndex]}");

    //final address = cartModel.address;
    /*printLog(
        "shippingMethodModel==> ${shippingMethodModel.shippingMethods[selectedIndex]}");
    printLog("totalMoney==> $totalMoney");
    printLog("cartModel==> $cartModel");
    printLog("address==> $address");
    Provider.of<CartModel>(context, listen: false)
        .setShippingMethod(shippingMethodModel.shippingMethods[selectedIndex]);*/
    //_loadShipping();
    if (shippingMethodModel?.shippingMethods?.isNotEmpty == true) {
      Provider.of<CartModel>(context, listen: false).setShippingMethod(
          shippingMethodModel.shippingMethods[selectedIndex]);
    }
    else{

      printLog("There is no shippingMethod");
      printLog("shippingMethodModel--> $shippingMethodModel");
      printLog("shippingMethods--> $shippingMethodModel.shippingMethods");
//      Services().widget.loadShippingMethods(context, address, true);
//      Provider.of<CartModel>(context, listen: false).setShippingMethod(
//          shippingMethodModel.shippingMethods[selectedIndex]);
    }
    //_loadShipping();
    addressdisplaytext = "View/Edit Shipping Address";
//    addressdisplaytext = address?.firstName ??
//        '' +
//            " " +
//            address?.lastName +
//            ", City : " +
//            address?.state +
//            "  . . . .";
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(width: 10),
        Text("Shipping Details",
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
        SizedBox(height: 10),
        Card(
          margin: const EdgeInsets.all(6.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
          elevation: 2.5,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
            child: kPaymentConfig['EnableShipping']
                ? ExpansionInfo(
                    rawTitle: addressdisplaytext ?? '',
                    expansionTitle: "Shipping Address",
                    children: <Widget>[
                      ShippingAddressInfo(),
                    ],
                  )
                : Container(),
          ),
        ),
        Card(
          margin: const EdgeInsets.all(6.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
          elevation: 2.5,
          child: ListenableProvider.value(
            value: shippingMethodModel,
            child: Consumer<ShippingMethodModel>(
              builder: (context, model, child) {
                if (model.isLoading) {
                  return Container(height: 100, child: kLoadingWidget(context));
                }

                if (model.message != null) {
                  return Container(
                    height: 100,
                    child: Center(
                        child: Text(model.message,
                            style: TextStyle(color: kErrorRed))),
                  );
                }
                model.shippingMethods.forEach((e) {
                  print('shippingMethods ${e.title}');
                });

                return Column(
                  children: <Widget>[
                    for (int i = 0; i < model.shippingMethods.length; i++)
                      if (model.shippingMethods[i].min_amount != null &&
                          totalMoney < model.shippingMethods[i].min_amount &&
                          model.shippingMethods[i].methodId == "free_shipping")
                        Container()
                      else
                        Column(
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                color: i == selectedIndex
                                    ? Theme.of(context).primaryColorLight
                                    : Colors.transparent,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 5),
                                child: Row(
                                  children: <Widget>[
                                    Radio(
                                      value: i,
                                      groupValue: selectedIndex,
                                      onChanged: (i) {
                                        printLog(
                                            "setShippingMethodi----> ${i}");
                                        setState(() {
                                          selectedIndex = i;
                                          printLog(
                                              "setShippingMethod----> ${selectedIndex}");
                                          Provider.of<CartModel>(context,
                                                  listen: false)
                                              .setShippingMethod(
                                                  shippingMethodModel
                                                          .shippingMethods[
                                                      selectedIndex]);
                                        });
                                      },
                                    ),
                                    SizedBox(width: 5),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(model.shippingMethods[i].title,
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  color: Theme.of(context)
                                                      .accentColor)),
                                          SizedBox(height: 2.5),
                                          if (model.shippingMethods[i].cost >
                                                  0.0 ||
                                              !isNotBlank(model
                                                  .shippingMethods[i]
                                                  .classCost))
                                            Text(
                                              Tools.getCurrecyFormatted(
                                                  model.shippingMethods[i].cost,
                                                  currency: currency),
                                              style: TextStyle(
                                                  fontSize: 13,
                                                  color: kGrey400),
                                            ),
                                          if (model.shippingMethods[i].cost ==
                                                  0.0 &&
                                              isNotBlank(model
                                                  .shippingMethods[i]
                                                  .classCost))
                                            Text(
                                              model
                                                  .shippingMethods[i].classCost,
                                              style: TextStyle(
                                                  fontSize: 13,
                                                  color: kGrey400),
                                            )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            i < model.shippingMethods.length - 1
                                ? Divider(height: 1)
                                : Container()
                          ],
                        )
                  ],
                );
              },
            ),
          ),
        ),
        SizedBox(height: 0),
        Container(
          child: SingleCheckoutReview(),
        ),

//        Container (
//          child: showPaymentScreen(context, widget, setLoading, newOrder),
//        )

        Container(
          child: SingleCheckoutPaymentMethods(
              onFinish: (order) {
                setState(() {
                  printLog("order---> ${order}");
                  newOrder = order;
                });
                Provider.of<CartModel>(context, listen: false).clearCart();
              },
              onLoading: setLoading,
              checkoutWidget: widget.checkoutWidget,
              paymentScreenContext: widget.paymentScreenContext),
        ),
      ],
    );
  }
}

  Widget showPaymentScreen(context, widget, setLoading, newOrder){

  Future.delayed(const Duration(milliseconds: 500), () {
   return  Container(
     child: SingleCheckoutPaymentMethods(
         onFinish: (order) {

             printLog("order---> ${order}");
             newOrder = order;

           Provider.of<CartModel>(context, listen: false).clearCart();
         },
         onLoading: setLoading,
         checkoutWidget: widget.checkoutWidget,
         paymentScreenContext: widget.paymentScreenContext),
   );

  });

}

class ShippingAddressInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cartModel = Provider.of<CartModel>(context);
    final address = cartModel.address;

    return Container(
      color: Theme.of(context).cardColor,
      padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 120,
                  child: Text(
                    "Name :",
                    style: TextStyle(
                      fontSize: 14,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                ),
                Expanded(
                  child: Text(
                    address.firstName +" "+address.lastName ,
                    style: TextStyle(
                      fontSize: 14,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                )
              ],
            ),
          ),
//          Padding(
//            padding: const EdgeInsets.symmetric(vertical: 5),
//            child: Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              crossAxisAlignment: CrossAxisAlignment.center,
//              children: <Widget>[
//                Container(
//                  width: 120,
//                  child: Text(
//                    S.of(context).lastName + " :",
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                ),
//                Expanded(
//                  child: Text(
//                    address?.lastName ?? '',
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                )
//              ],
//            ),
//          ),
          showEmail(context, address),
//          Padding(
//            padding: const EdgeInsets.symmetric(vertical: 5),
//            child: Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              crossAxisAlignment: CrossAxisAlignment.center,
//              children: <Widget>[
//                Container(
//                  width: 120,
//                  child: Text(
//                    S.of(context).email + " :",
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                ),
//                Expanded(
//
//                  child: Text(
//                    address?.email ?? '',
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                )
//              ],
//            ),
//          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 120,
                  child: Text(
                    S.of(context).streetName + " :",
                    style: TextStyle(
                      fontSize: 14,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                ),
                Expanded(
                  child: Text(
                    address?.street ?? '',
                    style: TextStyle(
                      fontSize: 14,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                )
              ],
            ),
          ),
          /*  Padding(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 120,
                  child: Text(
                    S.of(context).city + " :",
                    style: TextStyle(
                      fontSize: 14,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                ),
                Expanded(
                  child: Text(
                    address?.city ?? '',
                    style: TextStyle(
                      fontSize: 14,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                )
              ],
            ),
          ),*/
//          Padding(
//            padding: const EdgeInsets.symmetric(vertical: 5),
//            child: Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              crossAxisAlignment: CrossAxisAlignment.center,
//              children: <Widget>[
//                Container(
//                  width: 120,
//                  child: Text(
//                    "Area :",
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                ),
//                Expanded(
//                  child: Text(
//                    address?.state ?? '',
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                )
//              ],
//            ),
//          ),
//          Padding(
//            padding: const EdgeInsets.symmetric(vertical: 5),
//            child: Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              crossAxisAlignment: CrossAxisAlignment.center,
//              children: <Widget>[
//                Container(
//                  width: 120,
//                  child: Text(
//                    S.of(context).country + " :",
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                ),
//                Expanded(
//                  child: Text(
//                    CountryPickerUtils.getCountryByIsoCode(address.country)
//                            .name ??
//                        '',
//                    style: TextStyle(
//                      fontSize: 14,
//                      color: Theme.of(context).accentColor,
//                    ),
//                  ),
//                )
//              ],
//            ),
//          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 120,
                  child: Text(
                    S.of(context).phoneNumber + " :",
                    style: TextStyle(
                      fontSize: 14,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                ),
                Expanded(
                  child: Text(
                    address?.phoneNumber ?? '',
                    style: TextStyle(
                      fontSize: 14,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                )
              ],
            ),
          ),

//          Expanded(
//            child: ButtonTheme(
//              height: 45,
//              child: RaisedButton(
//                splashColor: ThemeColor,
//                color: Colors.green,
//                child: Text(
//                  "View / Edit Address",
//                  style: TextStyle(fontSize: 15.0, color: Colors.white),
//                ),
//                onPressed: () {
//                  updateAddress(context);
//                },
//              ),
//            ),
//          ),

//            Row(children: [
//              ButtonTheme(
//              height: 40,
//                child: RaisedButton(
//                splashColor: ThemeColor,
//                color: Colors.green,
//                  child: Text(
//                  "View / Edit Address",
//                  style: TextStyle(fontSize: 15.0, color: Colors.white),
//                  ),
//                onPressed: () {
//                updateAddress(context);
//                },
//                ),
//              )]),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    splashColor: ThemeColor,
                    color: Colors.green,
                    child: Text(
                      "View / Edit Address",
                      style: TextStyle(fontSize: 15.0, color: Colors.white),
                    ),
                    onPressed: () {
                      updateAddress(context);
                    },
                  ),
                ]),
          ),
          SizedBox(height: 0)
        ],
      ),
    );
  }

  Widget showEmail(context, address){

    printLog("address**** ---> ${address.email}");

    if(address.email.contains("guest")){

      return Container();
    }
    else{

      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 120,
              child: Text(
                S.of(context).email + " :",
                style: TextStyle(
                  fontSize: 14,
                  color: Theme.of(context).accentColor,
                ),
              ),
            ),
            Expanded(

              child: Text(
                address?.email ?? '',
                style: TextStyle(
                  fontSize: 14,
                  color: Theme.of(context).accentColor,
                ),
              ),
            )
          ],
        ),
      );
    }
  }

  void updateAddress(context) {

    Navigator.pushReplacement(
      context,MaterialPageRoute(builder: (context) => CheckoutAddressScreen()),);
//    Navigator.push(
//      context,
//      MaterialPageRoute(builder: (context) => CheckoutAddressScreen()),
//    );
//    Navigator.pop(context);
  }
}
