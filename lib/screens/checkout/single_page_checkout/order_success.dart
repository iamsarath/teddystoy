import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:teddystoy/common/constants/colors.dart';
import 'package:teddystoy/common/constants/general.dart';
import 'package:teddystoy/common/constants/loading.dart';
import 'package:teddystoy/generated/l10n.dart';
import 'package:teddystoy/models/cart/cart_model.dart';
import 'package:teddystoy/models/order/order.dart';
import 'package:teddystoy/models/user/user_model.dart';
import 'package:teddystoy/screens/orders/orders.dart';
import 'package:teddystoy/widgets/payment/myfatoorah/ui/transaction_details.dart';

class OrderSuccess extends StatefulWidget {
final Order order;

  OrderSuccess({this.order});

  @override
  _OrderSuccessState createState() => _OrderSuccessState();
}

class _OrderSuccessState extends State<OrderSuccess> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    final userModel = Provider.of<UserModel>(context);
    final screenSize = MediaQuery.of(context).size;
//    printLog("invoiceId order ${widget.order.invoiceId}");

    return Stack(
      children: <Widget>[
        Scaffold(
          backgroundColor: Theme.of(context).backgroundColor,
          appBar: AppBar(
            backgroundColor: Theme.of(context).backgroundColor,
            title: Text(
             "Order Success",
              style: TextStyle(
                color: Theme.of(context).accentColor,
                fontWeight: FontWeight.w400,
              ),
            ),

          ),
          body: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          child: ListView(
                            padding: const EdgeInsets.only(
                                top: 20, bottom: 10),
                            children: <Widget>[

                              Container(
                                width: screenSize.width,
                                child: FittedBox(
                                  fit: BoxFit.cover,
                                  child: Container(
                                    width:
                                    screenSize.width / (2 / (screenSize.height / screenSize.width)),
                                    child: Stack(
                                      children: <Widget>[
                                        Positioned(
                                          top: 0,
                                          right: 0,
                                          child: Image.asset(
                                            'assets/images/leaves.png',
                                            width: 120,
                                            height: 120,
                                          ),
                                        ),
                                        Column(
                                          children: <Widget>[
                                            SizedBox(height: 60),
                                            Text(S.of(context).orderSuccessTitle1,
                                                style: TextStyle(
                                                    fontSize: 27, color: Colors.green),
                                                textAlign: TextAlign.center),
                                            SizedBox(height: 20),
//                                            Padding(
//                                              padding: const EdgeInsets.symmetric(horizontal: 20),
//                                              child: Text(S.of(context).orderNo+"#${widget.order.number}",
//                                                  style: TextStyle(
//                                                      fontSize: 18, color: Colors.black),
//                                                  textAlign: TextAlign.center),
//                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 15),
                              Text(
                                S.of(context).orderSuccessMsg1,
                                style: TextStyle(
                                    color: Colors.black54, height: 1.4, fontSize: 14),
                              ),

//                              Container(
//                                margin: const EdgeInsets.only(top: 20),
//                                decoration: BoxDecoration(color: Theme.of(context).primaryColorLight),
//                                child: Padding(
//                                  padding: const EdgeInsets.all(15.0),
//                                  child: Column(
//                                    mainAxisAlignment: MainAxisAlignment.start,
//                                    crossAxisAlignment: CrossAxisAlignment.start,
//                                    children: <Widget>[
//                                      Text(
//                                        S.of(context).itsOrdered,
//                                        style: TextStyle(
//                                            fontSize: 18, color: Colors.black),
//                                      ),
//                                      SizedBox(height: 5),
//                                      Row(
//                                        mainAxisAlignment: MainAxisAlignment.center,
//                                        crossAxisAlignment: CrossAxisAlignment.center,
//                                        children: <Widget>[
//                                          Text(
//                                            S.of(context).orderNo,
//                                            style: TextStyle(
//                                                fontSize: 18, color: Colors.black),
//                                          ),
//                                          SizedBox(width: 5),
//                                          Expanded(
//                                            child: Text(
//                                              "#${widget.order.number}",
//                                              style: TextStyle(
//                                                  fontSize: 16, color: Colors.black),
//                                            ),
//                                          )
//                                        ],
//                                      )
//                                    ],
//                                  ),
//                                ),
//                              ),

                              if (userModel.user != null)
                                Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 30),
                                  child: Row(children: [
                                    Expanded(
                                      child: ButtonTheme(
                                        height: 45,
                                        child: RaisedButton(
                                          color: Colors.green,
                                          textColor: Colors.white,
                                          onPressed: () {

//                                            Navigator.of(context).pushNamed("/orders");
                                            Route route = MaterialPageRoute(
                                                builder: (context) => MyOrders());
                                            Navigator.pushReplacement(context, route);
                                          },
                                          child: Text(
                                            S.of(context).showAllMyOrdered.toUpperCase(),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ]),
                                ),

                            ],
                          ),
                        ),

                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),

      ],
    );
  }

}
