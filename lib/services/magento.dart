import 'dart:async';
import 'dart:convert' as convert;
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:quiver/strings.dart';
import 'package:teddystoy/models/banner/banner_model.dart';
import 'package:teddystoy/models/category/custom_category_model.dart';

import './helper/magento.dart';
import './index.dart';
import '../common/config.dart';
import '../common/constants.dart';
import '../models/address.dart';
import '../models/aftership.dart';
import '../models/blogs/blog_news.dart';
import '../models/cart/cart_model.dart';
import '../models/category/category.dart';
import '../models/coupon.dart';
import '../models/filter_attribute.dart';
import '../models/filter_tags.dart';
import '../models/order/order_model.dart';
import '../models/order/order_note.dart';
import '../models/payment_method.dart';
import '../models/product/product.dart';
import '../models/product/product_attribute.dart';
import '../models/product/product_variation.dart';
import '../models/review.dart';
import '../models/shipping_method.dart';
import '../models/user/user_model.dart';
import 'helper/blognews_api.dart';

class MagentoApi implements BaseServices {
  static final MagentoApi _instance = MagentoApi._internal();

  factory MagentoApi() => _instance;

  MagentoApi._internal();

  String domain;
  String accessToken;
  String guestQuoteId;
  Map<String, ProductAttribute> attributes;

  @override
  BlogNewsApi blogApi;

  void setAppConfig(appConfig) {
    domain = appConfig["url"];
    blogApi = BlogNewsApi(appConfig["blog"] ?? 'http://demo.mstore.io');
    accessToken = appConfig["accessToken"];
  }

  @override
  Future<List<BlogNews>> fetchBlogLayout({config, lang}) async {
    try {
      List<BlogNews> list = [];

      var endPoint = "posts?_embed&lang=$lang";
      if (config.containsKey("category")) {
        endPoint += "&categories=${config["category"]}";
      }
      if (config.containsKey("limit")) {
        endPoint += "&per_page=${config["limit"] ?? 20}";
      }

      var response = await blogApi.getAsync(endPoint);

      for (var item in response) {
        if (BlogNews.fromJson(item) != null) {
          list.add(BlogNews.fromJson(item));
        }
      }

      return list;
    } catch (e) {
      rethrow;
    }
  }

  Future<BlogNews> getPageById(int pageId) async {
    var response = await blogApi.getAsync("pages/$pageId?_embed");
    return BlogNews.fromJson(response);
  }

  Product parseProductFromJson(mainItem) {
    printLog("----fromMagentoJson --- ${mainItem} ");
    final dateSaleFrom = MagentoHelper.getCustomAttribute(
        mainItem["custom_attributes"], "special_from_date");
    final dateSaleTo = MagentoHelper.getCustomAttribute(
        mainItem["custom_attributes"], "special_to_date");
    bool onSale = false;
    var price = mainItem["price"];
    final salePrice = MagentoHelper.getCustomAttribute(
        mainItem["custom_attributes"], "special_price");
    var specialdateSaleFrom, specialdateSaleTo;
    if (dateSaleFrom != null) {
      var dateSaleFrom1 = DateTime.parse(dateSaleFrom);
      specialdateSaleFrom = dateSaleFrom1.add(Duration(hours: 23, minutes: 59));
    }
    if (dateSaleTo != null) {
      var dateSaleTo1 = DateTime.parse(dateSaleTo);
      specialdateSaleTo = dateSaleTo1.add(Duration(hours: 23, minutes: 59));
    }

    if (salePrice != null) {
      if (dateSaleFrom != null && dateSaleTo != null) {
        final now = DateTime.now();
        onSale =
            now.isAfter(specialdateSaleFrom) && now.isBefore(specialdateSaleTo);
        if (onSale == true) {
          price = MagentoHelper.getCustomAttribute(
              mainItem["custom_attributes"], "special_price");
        }
      } else if (dateSaleFrom != null && dateSaleTo == null) {
        final now = DateTime.now();
        onSale = now.isAfter(specialdateSaleFrom);
        if (onSale == true) {
          price = MagentoHelper.getCustomAttribute(
              mainItem["custom_attributes"], "special_price");
        }
      } else if (dateSaleFrom == null && dateSaleTo != null) {
        final now = DateTime.now();
        onSale = now.isBefore(specialdateSaleTo);
        if (onSale == true) {
          price = MagentoHelper.getCustomAttribute(
              mainItem["custom_attributes"], "special_price");
        }
      }
    }

    final mediaGalleryEntries = mainItem["media_gallery_entries"];
    var mediaGalleryVideoEntry;
    var images = [
      MagentoHelper.getProductImageUrl(domain, mainItem, "thumbnail")
    ];
    var videoThumbImage;
    if (mediaGalleryEntries != null && mediaGalleryEntries.length > 0) {
      for (var item in mediaGalleryEntries) {
        if (item["media_type"] == 'image') {
          printLog("here file ---> ${item["file"]}");
          images.add(
              MagentoHelper.getProductImageUrlByName(domain, item["file"]));
        }

        if (item["extension_attributes"] != null &&
            item["extension_attributes"] != '') {
          videoThumbImage =
              MagentoHelper.getProductImageUrlByName(domain, item["file"]);
          mediaGalleryVideoEntry =
              item["extension_attributes"]["video_content"]["video_url"];
        }
      }
    }
    bool inStock;

    if (mainItem["status"] == 1) {
      if (mainItem["extension_attributes"] != null &&
          mainItem["extension_attributes"].length > 0) {
        var salable_qty = mainItem["extension_attributes"]["salable_qty"];
        inStock = salable_qty > 0 ? true : false;
      }
    } else {
      inStock = false;
    }
    print("Product Status $inStock ");
    Product product = Product.fromMagentoJson(mainItem);
    final description = MagentoHelper.getCustomAttribute(
        mainItem["custom_attributes"], "description");
    product.description = description != null
        ? description
        : MagentoHelper.getCustomAttribute(
            mainItem["custom_attributes"], "short_description");
    product.inStock = inStock;
    product.onSale = onSale;
    product.price = "$price";
    product.regularPrice = "${mainItem["price"]}";
    product.salePrice = MagentoHelper.getCustomAttribute(
        mainItem["custom_attributes"], "special_price");
    product.images = images;
    product.imageFeature = images[0];
    product.videoUrl = mediaGalleryVideoEntry; //custom added code
    product.videoThumbImage = videoThumbImage;

    List<dynamic> categoryIds;
    if (mainItem["custom_attributes"] != null &&
        mainItem["custom_attributes"].length > 0) {
      for (var item in mainItem["custom_attributes"]) {
        if (item["attribute_code"] == "category_ids") {
          categoryIds = item["value"];
          break;
        }
      }
    }
    product.categoryId = categoryIds.isNotEmpty ? "${categoryIds[0]}" : "0";
    product.permalink = "";

    List<ProductAttribute> attrs = [];
    final options = mainItem["extension_attrmedia_gallery_entriesibutes"] !=
                null &&
            mainItem["extension_attributes"]["configurable_product_options"] !=
                null
        ? mainItem["extension_attributes"]["configurable_product_options"]
        : [];

    List attrsList = kAdvanceConfig["EnableAttributesConfigurableProduct"];
    List attrsLabelList =
        kAdvanceConfig["EnableAttributesLabelConfigurableProduct"];
    for (var i = 0; i < options.length; i++) {
      final option = options[i];

      for (var j = 0; j < attrsList.length; j++) {
        final item = attrsList[j];
        final itemLabel = attrsLabelList[j];
        if (option["label"].toLowerCase() ==
            itemLabel.toString().toLowerCase()) {
          List values = option["values"];
          List optionAttr = [];
          for (var f in attributes[item].options) {
            final value = values.firstWhere(
                (o) => o["value_index"].toString() == f["value"],
                orElse: () => null);
            if (value != null) {
              optionAttr.add(f);
            }
          }
          attrs.add(ProductAttribute.fromMagentoJson({
            "attribute_id": attributes[item].id,
            "attribute_code": attributes[item].name,
            "options": optionAttr
          }));
        }
      }
      attrsList.forEach((item) {});
    }

    product.attributes = attrs;
    product.type = mainItem["type_id"];
    return product;
  }

  Future getAllAttributes() async {
    try {
      attributes = Map<String, ProductAttribute>();
      List attrs = kAdvanceConfig["EnableAttributesConfigurableProduct"];
      attrs.forEach((item) async {
        ProductAttribute attrsItem = await getProductAttributes(item);
        attributes[item] = attrsItem;
      });
    } catch (e) {
      rethrow;
    }
  }

  Future<ProductAttribute> getProductAttributes(String attributeCode) async {
    try {
      var response = await http.get(
          MagentoHelper.buildUrl(domain, "products/attributes/$attributeCode"),
          headers: {'Authorization': 'Bearer ' + accessToken});

      return ProductAttribute.fromMagentoJson(
          convert.jsonDecode(response.body));
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<Category>> getCategories({lang}) async {
    try {
      print("magento.dart line 204------getCategories------");
      print(
          "magento.dart line 205------url------${MagentoHelper.buildUrl(domain, "mstore/categories", lang)}");
      print("magento.dart line 206------headers------${{
        'Authorization': 'Bearer ' + accessToken
      }}");
      var response = await http.get(
          MagentoHelper.buildUrl(domain, "mstore/categories", lang),
          headers: {'Authorization': 'Bearer ' + accessToken});
      List<Category> list = [];
      if (response.statusCode == 200) {
        print("magento.dart line 212------response.body------${response.body}");
        for (var item in convert.jsonDecode(response.body)["children_data"]) {
          if (item["is_active"] == true) {
            var category = Category.fromMagentoJson(item);
            category.parent = '0';
            if (item["image"] != null) {
              category.image = item["image"].toString().contains("pub/media")
                  ? "$domain/${item["image"]}"
                  : "$domain/pub/media/catalog/category/${item["image"]}";
            }
            list.add(category);

//            for (var item1 in item["children_data"]) {
//              if (item1["is_active"] == true) {
//                list.add(Category.fromMagentoJson(item1));
//
//                for (var item2 in item1["children_data"]) {
//                  if (item1["is_active"] == true) {
//                    list.add(Category.fromMagentoJson(item2));
//                  }
//                }
//              }
//            }
          }
        }
      }
      return list;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<BannerModel>> getBannerList({lang}) async {
    try {
      var response = await http.get(
          MagentoHelper.buildUrl(domain, "bannerslider/main-slider", lang),
          headers: {'Authorization': 'Bearer ' + accessToken});

      List<BannerModel> list = [];
      if (response.statusCode == 200) {
        for (var item in convert.jsonDecode(response.body)) {
          var banner = BannerModel.fromMagentoJson(item);
          list.add(banner);
        }
      }
      return list;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<CustomCategoryModel>> getCustomCategoriesList({lang}) async {
    try {
      var response = await http.get(
          MagentoHelper.buildUrl(
              domain, "bannerslider/home-category-grid", lang),
          headers: {'Authorization': 'Bearer ' + accessToken});

      List<CustomCategoryModel> list = [];
      if (response.statusCode == 200) {
        print(
            "cat url---> ${MagentoHelper.buildUrl(domain, "bannerslider/home-category-grid", lang)}");
        print("cat response---> ${response.body}");
        for (var item in convert.jsonDecode(response.body)) {
          var banner = CustomCategoryModel.fromMagentoJson(item);
          list.add(banner);
        }
      }
      return list;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<Product>> getProducts() async {
    try {
      print(
          "line 247 magento.dart-------getProducts url-------${MagentoHelper.buildUrl(domain, "mstore/products&searchCriteria[pageSize]=$ApiPageSize")}");
      var response = await http.get(
          MagentoHelper.buildUrl(
              domain, "mstore/products&searchCriteria[pageSize]=$ApiPageSize"),
          headers: {'Authorization': 'Bearer ' + accessToken});
      List<Product> list = [];
      if (response.statusCode == 200) {
        print("line 254 magento.dart-------getProducts-------${response.body}");
        for (var item in convert.jsonDecode(response.body)["items"]) {
          list.add(parseProductFromJson(item));
        }
      }
      return list;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<Product>> fetchProductsLayout({config, lang}) async {
    try {
      print("magento.dart ------product list------");
      List<Product> list = [];
      if (config["layout"] == "imageBanner" ||
          config["layout"] == "circleCategory") {
        return list;
      }

      var endPoint = "?";
      if (config.containsKey("category")) {
        endPoint +=
            "searchCriteria[filter_groups][0][filters][0][field]=category_id&searchCriteria[filter_groups][0][filters][0][value]=${config["category"]}&searchCriteria[filter_groups][0][filters][0][condition_type]=eq&searchCriteria[pageSize]=$ApiPageSize";
      }
      endPoint +=
          "&searchCriteria[filter_groups][1][filters][0][field]=visibility&searchCriteria[filter_groups][1][filters][0][value]=4";

      var response = await http.get(
          MagentoHelper.buildUrl(domain, "mstore/products$endPoint", lang),
          headers: {'Authorization': 'Bearer ' + accessToken});

      print("magento.dart line 286 ------product list------${response.body}");
      if (response.statusCode == 200) {
        for (var item in convert.jsonDecode(response.body)["items"]) {
          list.add(parseProductFromJson(item));
        }
      }
      return list;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<Product>> fetchProductsByCategory({
    categoryId,
    tagId,
    page,
    minPrice,
    maxPrice,
    lang,
    orderBy,
    order,
    featured,
    onSale,
    attribute,
    attributeTerm,
  }) async {
    try {
      print("line 341 magento.dart ------fetchProductsByCategory------");
      var endPoint = "?";
      if (categoryId != null) {
        endPoint +=
            "searchCriteria[filter_groups][0][filters][0][field]=category_id&searchCriteria[filter_groups][0][filters][0][value]=$categoryId&searchCriteria[filter_groups][0][filters][0][condition_type]=eq";
      }
      if (maxPrice != null) {
        endPoint +=
            "&searchCriteria[filter_groups][0][filters][1][field]=price&searchCriteria[filter_groups][0][filters][1][value]=$maxPrice&searchCriteria[filter_groups][0][filters][1][condition_type]=lteq";
      }
      if (page != null) {
        endPoint += "&searchCriteria[currentPage]=$page";
      }
      if (orderBy != null) {
        endPoint +=
            "&searchCriteria[sortOrders][1][field]=${orderBy == "date" ? "created_at" : orderBy}";
      }
      if (order != null) {
        endPoint +=
            "&searchCriteria[sortOrders][1][direction]=${(order as String).toUpperCase()}";
      }
      endPoint += "&searchCriteria[pageSize]=$ApiPageSize";

      endPoint +=
          "&searchCriteria[filter_groups][1][filters][0][field]=visibility&searchCriteria[filter_groups][1][filters][0][value]=4";

      print(
          "line 335 magento.dart ------fetchProductsByCategory url ------> ${MagentoHelper.buildUrl(domain, "mstore/products$endPoint", lang)}");
      print(
          "line 336 magento.dart ------fetchProductsByCategory header ------> ${"{'Authorization': 'Bearer ' + ${accessToken}}"}");
      var response = await http.get(
          MagentoHelper.buildUrl(domain, "mstore/products$endPoint", lang),
          headers: {'Authorization': 'Bearer ' + accessToken});

      print(
          "magento.dart line 348 ------fetchProductsByCategory------${response.body}");
      List<Product> list = [];
      if (response.statusCode == 200) {
        for (var item in convert.jsonDecode(response.body)["items"]) {
          list.add(parseProductFromJson(item));
        }
      }
      return list;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<User> loginFacebook({String token}) async {
    try {
      var response = await http.post(
          MagentoHelper.buildUrl(domain, "mstore/social_login"),
          body: convert.jsonEncode({"token": token, "type": "facebook"}),
          headers: {"content-type": "application/json"});

      if (response.statusCode == 200) {
        final token = convert.jsonDecode(response.body);
        return await getUserInfo(token);
      } else {
        final body = convert.jsonDecode(response.body);
        throw Exception(body["message"] != null
            ? MagentoHelper.getErrorMessage(body)
            : "Can not get token");
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<User> loginSMS({String token}) async {
    try {
      var response = await http.post(
        MagentoHelper.buildUrl(domain, "mstore/social_login"),
        body: convert.jsonEncode({"token": token, "type": "firebase_sms"}),
        headers: {"content-type": "application/json"},
      );

      if (response.statusCode == 200) {
        final token = convert.jsonDecode(response.body);
        return await getUserInfo(token);
      } else {
        final body = convert.jsonDecode(response.body);
        throw Exception(body["message"] != null
            ? MagentoHelper.getErrorMessage(body)
            : "Can not get token");
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<List<Review>> getReviews(productId) {
    return null;
  }

  @override
  Future<List<ProductVariation>> getProductVariations(Product product) async {
    try {
      final res = await http.get(
          MagentoHelper.buildUrl(
              domain, "configurable-products/${product.sku}/children"),
          headers: {
            'Authorization': 'Bearer ' + accessToken,
            "content-type": "application/json"
          });

      List<ProductVariation> list = [];
      if (res.statusCode == 200) {
        for (var item in convert.jsonDecode(res.body)) {
          list.add(ProductVariation.fromMagentoJson(item));
        }
      }

      return list;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<ShippingMethod>> getShippingMethods(
      {Address address, String token, String checkoutId}) async {

    print("magento.dart 555-----region_id----> ${address.state_id} ");
    print("magento.dart 556-----token----> ${token} ");
    print("magento.dart 557-----checkoutId----> ${checkoutId} ");

    try {
      print(
          "magento.dart line 435-----Shipping methods url----> ${MagentoHelper.buildUrl(domain, "carts/mine/estimate-shipping-methods")}");
      print(
          "magento.dart line 436-----Shipping methods guest-carts url----> ${MagentoHelper.buildUrl(domain, "guest-carts/$guestQuoteId/estimate-shipping-methods")}");
      String url = token != null
          ? MagentoHelper.buildUrl(
              domain, "carts/mine/estimate-shipping-methods")
          : MagentoHelper.buildUrl(
              domain, "guest-carts/$guestQuoteId/estimate-shipping-methods");
      print("magento.dart -----Shipping methods1 urlnew----> ${url} ");
      final res = await http.post(url,
          body: convert.jsonEncode({
            "address": {
              "region_id": address.state_id,
              "country_id": address.country
            }
          }),
          headers: token != null
              ? {
                  'Authorization': 'Bearer ' + token,
                  "content-type": "application/json"
                }
              : {"content-type": "application/json"});

//      print("magento.dart line 576-----Shipping methods token----> ${token}");
      print("magento.dart line 577-----Shipping methods body----> ${{
        "address": {
          "region_id": address.state_id,
          "country_id": address.country
        }
      }}");
//      print("magento.dart line 459-----Shipping methods headers----> ${{
//        'Authorization': 'Bearer ' + token,
//        "content-type": "application/json"
//      }}");

      if (res.statusCode == 200) {
        print("SHIPPING METHOD SUCCESS*********** ----> ${res.body}");
        List<ShippingMethod> list = [];
        for (var item in convert.jsonDecode(res.body)) {
          list.add(ShippingMethod.fromMagentoJson(item));
        }
        return list;
      } else {
        debugPrint("Error Resp----> ${res}");
        throw Exception("Can not get shipping methods");
        print("error");
      }
    } catch (err) {
      print("err--> $err");
      rethrow;
    }
  }

  @override
  Future<List<PaymentMethod>> getPaymentMethods(
      {Address address, ShippingMethod shippingMethod, String token}) async {
    printLog("getPaymentMethods address ----> ${address}");
    printLog("getPaymentMethods shippingMethod ----> ${shippingMethod}");
    printLog("getPaymentMethods token ----> ${token}");
    try {
      final params = {
        "addressInformation": {
          "shipping_address": address.toMagentodataJson()["address"],
          "billing_address": address.toMagentodataJson()["address"],
          "shipping_carrier_code": shippingMethod.id,
          "shipping_method_code": shippingMethod.method_code
        }
      };
      String url = token != null
          ? MagentoHelper.buildUrl(domain, "carts/mine/shipping-information")
          : MagentoHelper.buildUrl(
              domain, "guest-carts/$guestQuoteId/shipping-information");

      print("magento.dart line 538----- url getPaymentMethods----> ${url}");
      final res = await http.post(url,
          body: convert.jsonEncode(params),
          headers: token != null
              ? {
                  'Authorization': 'Bearer ' + token,
                  "content-type": "application/json"
                }
              : {"content-type": "application/json"});

      print(
          "magento.dart line 548----- response getPaymentMethods----> ${res.body}");
      if (res.statusCode == 200) {
        List<PaymentMethod> list = [];
        for (var item in convert.jsonDecode(res.body)["payment_methods"]) {
          list.add(PaymentMethod.fromMagentoJson(item));
        }
        return list;
      } else {
        throw Exception("Can not get payment methods");
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<List<Order>> getMyOrders({UserModel userModel, int page}) async {
    try {
      var endPoint = "?";
      endPoint +=
          "searchCriteria[filter_groups][0][filters][0][field]=customer_email&searchCriteria[filter_groups][0][filters][0][value]=${userModel.user.email}&searchCriteria[filter_groups][0][filters][0][condition_type]=eq";
      endPoint += "&searchCriteria[currentPage]=0";
      endPoint += "&searchCriteria[pageSize]=$ApiPageSize";

      printLog(
          "magento.dart line 556---Order History api ==> ${MagentoHelper.buildUrl(domain, "orders$endPoint")}");
      printLog(
          "magento.dart line 556---Order History accessToken ==> ${accessToken}");
      var response = await http.get(
          MagentoHelper.buildUrl(domain, "orders$endPoint"),
          headers: {'Authorization': 'Bearer ' + accessToken});

      List<Order> list = [];
      printLog("magento.dart line 561---Order History ==> ${response.body}");
      if (response.statusCode == 200) {
        for (var item in convert.jsonDecode(response.body)["items"]) {
          list.add(Order.fromMagentoJson(item));
        }
      }

      //sorting the list to get latest orders displayed first
//      list.sort((invoice_1, invoice_2) {
//        var orderNumber_1 = invoice_1.number;
//        var orderNumber_2 = invoice_2.number;
//        return orderNumber_2.compareTo(orderNumber_1);
//      });

      return list;
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<Order> createOrder(
      {CartModel cartModel, UserModel user, bool paid}) async {
    try {
      bool isGuest = user.user == null || user.user.cookie == null;
      String url = !isGuest
          ? MagentoHelper.buildUrl(domain, "carts/mine/payment-information")
          : MagentoHelper.buildUrl(
              domain, "guest-carts/$guestQuoteId/payment-information");
      var params = Order().toMagentoJson(cartModel, null, paid);
      if (isGuest) {
        params["email"] = cartModel.address.email;
        params["firstname"] = cartModel.address.firstName;
        params["lastname"] = cartModel.address.lastName;
      } else {
        Address address = cartModel.address;
        address.toMagentoJson().remove('city');
        //params["billing_address"]["house_no"] = "12345";
        //params["shipping_address"]["house_no"] = "12345";
      }

      printLog("line 704 magento.dart url createOrder---> ${url}");
      printLog("line 705 magento.dart url params---> ${params}");
      final res = await http.post(url,
          body: convert.jsonEncode(params),
          headers: !isGuest
              ? {
                  'Authorization': 'Bearer ' + user.user.cookie,
                  "content-type": "application/json"
                }
              : {"content-type": "application/json"});

      printLog("line 715 magento.dart res.body createOrder---> ${res.body}");

      final body = convert.jsonDecode(res.body);
      if (res.statusCode == 200) {
        var order = Order();
        order.id = body.toString();
        order.number = body.toString();
        return order;
      } else {
        if (body["message"] != null) {
          throw Exception(MagentoHelper.getErrorMessage(body));
        } else {
          throw Exception("Can not create order");
        }
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future updateOrder(orderId, {status, token}) async {
    try {
      var response = await http.post(
//        MagentoHelper.buildUrl(domain, "mstore/me/orders/$orderId/cancel"),
        MagentoHelper.buildUrl(domain, "orders/$orderId/cancel"),
        body: convert.jsonEncode({}),
//        headers: {
//          'Authorization': 'Bearer ' + token,
//          "content-type": "application/json"
//        },
        headers: {
          'Authorization': 'Bearer ' + accessToken,
          "content-type": "application/json"
        },
      );
      final body = convert.jsonDecode(response.body);
      printLog(
          "magento.dart line  662---Cancel Order---> ${MagentoHelper.buildUrl(domain, "orders/$orderId/cancel")}");
      MagentoHelper.buildUrl(domain, "orders/$orderId/cancel");

      printLog("magento.dart line  666---token Order ---> ${accessToken}");
      printLog(
          "magento.dart line  667---Cancel Order response.body---> ${response.body}");
      if (body is Map && body["message"] != null) {
        throw Exception(MagentoHelper.getErrorMessage(body));
      } else {
        return;
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<List<Product>> searchProducts(
      {name, categoryId, tag, attribute, attributeId, page, lang}) async {
    try {
      var endPoint = "?";
      if (name != null) {
        endPoint +=
            "searchCriteria[filter_groups][0][filters][0][field]=name&searchCriteria[filter_groups][0][filters][0][value]=%$name%&searchCriteria[filter_groups][0][filters][0][condition_type]=like";
      }
      if (page != null) {
        endPoint += "&searchCriteria[currentPage]=$page";
      }
      endPoint += "&searchCriteria[pageSize]=$ApiPageSize";

      var response = await http.get(
          MagentoHelper.buildUrl(domain, "mstore/products$endPoint"),
          headers: {'Authorization': 'Bearer ' + accessToken});

      List<Product> list = [];
      if (response.statusCode == 200) {
        for (var item in convert.jsonDecode(response.body)["items"]) {
          list.add(parseProductFromJson(item));
        }
      }
      return list;
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<User> createUser({firstName, lastName, username, password}) async {
    try {
      print(
          'magento.dart line 624------>${MagentoHelper.buildUrl(domain, "customers")}');
      var response =
          await http.post(MagentoHelper.buildUrl(domain, "customers"),
              body: convert.jsonEncode({
                "customer": {
                  "email": username,
                  "firstname": firstName,
                  "lastname": lastName
                },
                "password": password
              }),
              headers: {"content-type": "application/json"});

      print(
          'magento.dart line 637 response.statusCode------>${response.statusCode}');
      print('magento.dart line 638 response.body------>${response.body}');
      if (response.statusCode == 200) {
        return await login(username: username, password: password);
      } else {
        final body = convert.jsonDecode(response.body);
        throw Exception(body["message"] != null
            ? MagentoHelper.getErrorMessage(body)
            : "Can not get token");
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<User> getUserInfo(cookie) async {
    var res = await http.get(MagentoHelper.buildUrl(domain, "customers/me"),
        headers: {'Authorization': 'Bearer ' + cookie});
    return User.fromMagentoJsonFB(convert.jsonDecode(res.body), cookie);
  }

  @override
  Future<User> login({username, password}) async {
    try {
      var response = await http.post(
          MagentoHelper.buildUrl(domain, "integration/customer/token"),
          body:
              convert.jsonEncode({"username": username, "password": password}),
          headers: {"content-type": "application/json"});

      if (response.statusCode == 200) {
        final token = convert.jsonDecode(response.body);
        return await getUserInfo(token);
      } else {
        final body = convert.jsonDecode(response.body);
        throw Exception(body["message"] != null
            ? MagentoHelper.getErrorMessage(body)
            : "Can not get token");
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<User> loginApple({String email, String fullName}) async {
    try {
      final lastName =
          fullName.split(" ").length > 1 ? fullName.split(" ")[1] : "fluxstore";
      var response = await http.post(
        MagentoHelper.buildUrl(domain, "mstore/appleLogin"),
        body: convert.jsonEncode({
          "email": email,
          "firstName": fullName.split(" ")[0],
          "lastName": lastName
        }),
        headers: {"content-type": "application/json"},
      );

      if (response.statusCode == 200) {
        final token = convert.jsonDecode(response.body);
        return await getUserInfo(token);
      } else {
        final body = convert.jsonDecode(response.body);
        throw Exception(body["message"] != null
            ? MagentoHelper.getErrorMessage(body)
            : "Can not get token");
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<Product> getProduct(id) async {
    return null;
  }

  Future<bool> addToCart(CartModel cartModel, String token, quoteId,
      {isDelete = false, guestCartId}) async {
    try {
      //delete items in cart
      if (isDelete) {
        await Future.forEach(cartModel.productsInCart.keys, (key) async {
          String productId = Product.cleanProductID(key);

          await http.delete(
              MagentoHelper.buildUrl(domain, "carts/mine/items/$productId"),
              headers: {'Authorization': 'Bearer $token'});
        });
        await http.delete(MagentoHelper.buildUrl(domain, "carts/mine/coupons"),
            headers: {'Authorization': 'Bearer $token'});
      }
      //add items to cart
      await Future.forEach(cartModel.productsInCart.keys, (key) async {
        Map<String, dynamic> params = Map<String, dynamic>();
        params["qty"] = cartModel.productsInCart[key];
        params["quote_id"] = quoteId;
        params["sku"] = cartModel.productSkuInCart[key];
        final res = await http.post(
            guestCartId == null
                ? MagentoHelper.buildUrl(domain, "carts/mine/items")
                : MagentoHelper.buildUrl(
                    domain, "guest-carts/$guestCartId/items"),
            body: convert.jsonEncode({"cartItem": params}),
            headers: token != null
                ? {
                    'Authorization': 'Bearer ' + token,
                    "content-type": "application/json"
                  }
                : {"content-type": "application/json"});
        final body = convert.jsonDecode(res.body);
        printLog(
            "magento.dart 925 addToCart url---> ${MagentoHelper.buildUrl(domain, "carts/mine/items")}");
        printLog(
            "magento.dart 926 addToCart url---> ${MagentoHelper.buildUrl(domain, "guest-carts/$guestCartId/items")}");
        printLog("magento.dart 926 addToCart params---> ${{"cartItem": params}}");
        String message = body["message"];
        if (body["messages"] != null &&
            body["messages"]["error"] != null &&
            body["messages"]["error"][0].length > 0) {
          printLog(
              "addToCart Error---> ${body["messages"]["error"][0]["message"]}");
          throw Exception(body["messages"]["error"][0]["message"]);
        } else {
          print("addToCart : ${convert.jsonDecode(res.body)}");
          String msgBody = "${convert.jsonDecode(res.body)}";
          if(msgBody.contains("Product that you are trying to add is not available") ||
              msgBody.contains("The requested qty is not available")){

            print("===> Pdt out of stock");
          }
          else{

            print("===> Pdt in stock");
//            return;
          }
          return;
        }

        /*else if (message.contains("The requested qty is not available")) {
        throw Exception(body["message"]);
        }*/
      });
      return true;
    } catch (err) {
      rethrow;
    }
  }

  Future<bool> addItemsToCart(CartModel cartModel, String token) async {
    printLog("-- addItemsToCart -- $token");
    try {
      if (token != null) {
        //get cart info
        var res = await http.get(MagentoHelper.buildUrl(domain, "carts/mine"),
            headers: {'Authorization': 'Bearer ' + token});
        final cartInfo = convert.jsonDecode(res.body);
        if (res.statusCode == 200) {
          return await addToCart(cartModel, token, cartInfo["id"],
              isDelete: true);
        } else if (res.statusCode == 401) {
          throw Exception("Token expired. Please logout then login again");
        } else if (res.statusCode != 404) {
          throw Exception(MagentoHelper.getErrorMessage(cartInfo));
        }
      }

      //create a quote
      String url = token != null
          ? MagentoHelper.buildUrl(domain, "carts/mine")
          : MagentoHelper.buildUrl(domain, "guest-carts");
      var res = await http.post(url,
          headers: token != null ? {'Authorization': 'Bearer ' + token} : {});
      if (res.statusCode == 200) {
        if (token != null) {
          final quoteId = convert.jsonDecode(res.body);
          return await addToCart(cartModel, token, quoteId);
        } else {
          String quoteId = convert.jsonDecode(res.body);
          var response = await http
              .get(MagentoHelper.buildUrl(domain, "guest-carts/$quoteId"));
          final cartInfo = convert.jsonDecode(response.body);
          if (response.statusCode == 200) {
            final cartId = cartInfo["id"];
            guestQuoteId = quoteId;
            return await addToCart(cartModel, token, quoteId,
                guestCartId: cartId);
          } else {
            throw Exception(MagentoHelper.getErrorMessage(cartInfo));
          }
        }
      } else {
        throw Exception(
            MagentoHelper.getErrorMessage(convert.jsonDecode(res.body)));
      }
    } catch (err) {
      rethrow;
    }
  }

  Future<bool> updateMyFatoorahTransactionDetails(
      {context, orderId, paymentId}) async {
    try {
      var response = await http.get(
          MagentoHelper.buildUrl(domain,
              "myfatoorahpaymentgateway/transactionupdate/$orderId/$paymentId"),
          headers: {'Authorization': 'Bearer ' + accessToken});
      printLog(
          "updateTransactionDetails url---> ${MagentoHelper.buildUrl(domain, "myfatoorahpaymentgateway/transactionupdate/$orderId/$paymentId")}");
      printLog("updateTransactionDetails response---> ${response.body}");
      if (response.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<double> applyCoupon(String token, String coupon) async {
    try {
      String url = token != null
          ? MagentoHelper.buildUrl(domain, "carts/mine/coupons/$coupon")
          : MagentoHelper.buildUrl(
              domain, "guest-carts/$guestQuoteId/coupons/$coupon");
      var res = await http.put(url,
          headers: token != null ? {'Authorization': 'Bearer ' + token} : {});
      var body = convert.jsonDecode(res.body);
      if (res.statusCode == 200) {
        String totalUrl = token != null
            ? MagentoHelper.buildUrl(domain, "carts/mine/totals")
            : MagentoHelper.buildUrl(
                domain, "guest-carts/$guestQuoteId/totals");
        var res = await http.get(totalUrl,
            headers: token != null ? {'Authorization': 'Bearer ' + token} : {});

        print("magento.dart line 918 ------applyCoupon------${totalUrl}");
        print(
            "magento.dart line 918 ------applyCoupon res.body------${res.body}");
        body = convert.jsonDecode(res.body);
        if (body['message'] != null) {
          throw Exception(MagentoHelper.getErrorMessage(body));
        } else {
          double discount = double.parse("${body['discount_amount']}");
          return discount < 0 ? discount * (-1) : discount;
        }
      } else {
        throw Exception(MagentoHelper.getErrorMessage(body));
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<Coupons> getCoupons() async {
    try {
      return Coupons.getListCoupons([]);
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<AfterShip> getAllTracking() {
    return null;
  }

  @override
  Future<List<OrderNote>> getOrderNote(
      {UserModel userModel, String orderId}) async {
    return null;
  }

  @override
  Future<Null> createReview(
      {String productId, Map<String, dynamic> data}) async {}

  @override
  Future<Map<String, dynamic>> getHomeCache() {
    // ignore: avoid_returning_null_for_future
    return null;
  }

  @override
  Future<User> loginGoogle({String token}) async {
    try {
      var response = await http.post(
          MagentoHelper.buildUrl(domain, "mstore/social_login"),
          body: convert.jsonEncode({"token": token, "type": "google"}),
          headers: {"content-type": "application/json"});

      if (response.statusCode == 200) {
        final token = convert.jsonDecode(response.body);
        return await getUserInfo(token);
      } else {
        final body = convert.jsonDecode(response.body);
        throw Exception(body["message"] != null
            ? MagentoHelper.getErrorMessage(body)
            : "Can not get token");
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<Map<String, dynamic>> updateUserInfo(
      Map<String, dynamic> json, String token) async {
    try {
      if (isNotBlank(json["user_email"])) {
        var response = await http.post(
          MagentoHelper.buildUrl(domain, "mstore/customers/me/changeEmail"),
          body: convert.jsonEncode({
            "new_email": json["user_email"],
            "current_password": json["current_pass"]
          }),
          headers: {
            'Authorization': 'Bearer token',
            "content-type": "application/json"
          },
        );
        final body = convert.jsonDecode(response.body);
        if (body is Map && body["message"] != null) {
          throw Exception(MagentoHelper.getErrorMessage(body));
        }
      }
      if (isNotBlank(json["user_pass"])) {
        var response = await http.post(
          MagentoHelper.buildUrl(domain, "mstore/customers/me/changePassword"),
          body: convert.jsonEncode({
            "new_password": json["user_pass"],
            "confirm_password": json["user_pass"],
            "current_password": json["current_pass"]
          }),
          headers: {
            'Authorization': 'Bearer ' + token,
            "content-type": "application/json"
          },
        );
        final body = convert.jsonDecode(response.body);
        if (body is Map && body["message"] != null) {
          throw Exception(MagentoHelper.getErrorMessage(body));
        }
      }
      return json;
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future getCategoryWithCache() {
    // TO-DO: implement getCategoryCache
    return null;
  }

  @override
  Future<List<FilterAttribute>> getFilterAttributes() {
    // TO-DO: implement getFilterAttributes
    throw UnimplementedError();
  }

  @override
  Future<List<SubAttribute>> getSubAttributes({int id}) {
    // TO-DO: implement getAttributes
    throw UnimplementedError();
  }

  @override
  Future<List<FilterTag>> getFilterTags() {
    return null;
  }

  @override
  Future<String> getCheckoutUrl(Map<String, dynamic> params) {
    // TODO: implement getCheckoutUrl
    return null;
  }

  @override
  Future<String> submitForgotPassword(
      {String forgotPwLink, Map<String, dynamic> data}) {
    // TODO: implement submitForgotPassword
    return null;
  }

  @override
  Future logout() {
    // TODO: implement logout
    return null;
  }
}
